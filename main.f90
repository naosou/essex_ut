program main
  use iso_c_binding
  use coefficient
  use execarguments
  use mod_parallel
  !$ use omp_lib
  implicit none
  include 'mpif.h'
  type CRS_mat_alloc
     double precision ,allocatable :: val(:),  diag(:)
     integer, allocatable :: col_ind(:) ,row_ptr(:)
  end type CRS_mat_alloc
  integer s_len, i_flag
  integer data_type, data_type_getc, bsize
  integer n_row, n_val, i, j, Bn_row, Bn_val, bnd_min, bnd_max, status_ord, n_len
  integer :: temp_size(1)
  integer, allocatable, target :: row_ptr(:), col_ind(:)
  double precision, allocatable, target :: val(:), b(:), x(:), r(:)
  type(c_ptr) :: c_row_ptr, c_col_ind, c_val
  double precision shift
  real(8) avg
  type(CRS_mat) matA, IC
  type(CRS_mat_alloc) matB
  type(BCRS_mat) B_IC
  type(argflags) :: aflags
  type(ord_para) :: abmc
  type(row_sepa) :: rrange
  real(8) st_time, ed_time
  !real(c_double), allocatable, target :: val(:)
  logical flag_row, flag_both, shift_flag
  character(1000) :: fname_A, fname_B
  real(8), allocatable :: temp_real(:)

  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr
  integer, allocatable :: aflags_ints(:)
  
  interface
     
     ! integer (c_int) function read_binCRS (matrixPath, n, nnz, val, row_ptr, col_idx) BIND(C, NAME="read_binCRS")
     !   use, intrinsic :: iso_c_binding
     !   implicit none
     !   character(c_char) :: matrixPath
     !   integer (c_int64_t), intent(out) :: n, nnz
     !   type(c_ptr) :: val
     !   type(c_ptr) :: row_ptr, col_idx
     ! end function read_binCRS

     subroutine read_mm_form(fname, n_row, n_val, val, row_ptr, col_ind)
       integer, intent(out) :: n_row, n_val
       integer, allocatable, intent(inout) :: row_ptr(:), col_ind(:)
       double precision, allocatable, intent(inout) :: val(:)
       character(1000), intent(in) :: fname
     end subroutine read_mm_form

     subroutine read_vec(fname, n_row, b)
       integer, intent(in) :: n_row
       double precision, allocatable, intent(inout) :: b(:)
       character(1000), intent(in) :: fname
     end subroutine read_vec

  end interface

  call MPI_Init(ierr)
  call MPI_Comm_size(MCW ,num_proc ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1
  
  allocate(aflags_ints(aflags%num_int))
  
  if(me_proc == 1) then
     call read_args(fname_A, fname_B, aflags)
     ! shift_flag = 0
     aflags_ints(1) = aflags%read_type
     aflags_ints(2) = aflags%solver
     aflags_ints(3) = aflags%shift_method
     aflags_ints(4) = aflags%normalize
     aflags_ints(5) = aflags%r_vec
     aflags_ints(6) = aflags%bsize
     aflags_ints(7) = aflags%data_type
     aflags_ints(8) = aflags%s_mod
     aflags_ints(9) = aflags%color
     aflags_ints(10) = aflags%grping
     aflags_ints(11) = aflags%color_method
     
     select case (aflags%read_type)
     case(1)
        call read_mm_form(fname_A, n_row, n_val, val, row_ptr, col_ind)
        write(*, *) 'Number of rows', n_row, ', non-zero', n_val
        matA%row_ptr => row_ptr
        matA%col_ind => col_ind
        matA%val => val  
     case(2)
        ! data_type_getc = read_binCRS(fname, n_row, n_val, c_val, c_row_ptr, c_col_ind)
        ! write(*, *) 'Check result2', data_type_getc, n_row, n_val
        ! temp_size(1) = n_val
        ! call C_F_pointer(c_val, matA%val, temp_size)
        ! call C_F_pointer(c_col_ind, matA%col_ind, temp_size)
        ! temp_size(1) = n_row+1
        ! call C_F_pointer(c_row_ptr, matA%row_ptr, temp_size)
        ! matA%row_ptr = matA%row_ptr + 1
        ! matA%col_ind = matA%col_ind + 1
        stop
     end select

  endif
  call MPI_Barrier(MCW, ierr)

  call MPI_Bcast(aflags_ints, aflags%num_int, MPI_INTEGER, 0, MCW, ierr)
  call MPI_Bcast(aflags%shift_r, 1, MPI_DOUBLE_PRECISION, 0, MCW, ierr)
  call MPI_Bcast(aflags%shift_c, 1, MPI_COMPLEX, 0, MCW, ierr)
  call MPI_Bcast(n_row, 1, MPI_INTEGER, 0, MCW, ierr)
  call MPI_Bcast(n_val, 1, MPI_INTEGER, 0, MCW, ierr)

  ! call init_bound(n_row, num_proc, me_proc, bnd_min, bnd_max, aflags)
  
  if(me_proc /= 1) then
     aflags%read_type    = aflags_ints(1) 
     aflags%solver       = aflags_ints(2) 
     aflags%shift_method = aflags_ints(3) 
     aflags%normalize    = aflags_ints(4) 
     aflags%r_vec        = aflags_ints(5) 
     aflags%bsize        = aflags_ints(6) 
     aflags%data_type    = aflags_ints(7) 
     aflags%s_mod        = aflags_ints(8)
     aflags%color        = aflags_ints(9)
     aflags%grping       = aflags_ints(10)
     aflags%color_method = aflags_ints(11)
     allocate(matA%row_ptr(n_row+1), matA%col_ind(n_val), matA%val(n_val))
  endif

  call MPI_Bcast(matA%row_ptr, n_row+1, MPI_INTEGER, 0, MCW, ierr)
  call MPI_Bcast(matA%col_ind, n_val,   MPI_INTEGER, 0, MCW, ierr)
  call MPI_Bcast(matA%val, n_val, MPI_Double_precision, 0, MCW, ierr)

  allocate(b(n_row), x(n_row))

  ! do j = 1, n_row
  !    if(5401 <= j .and. j <= 5404) then
  !       do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
  !          write(*, *) j, matA%col_ind(i)
  !          if(matA%col_ind(i) > 5405) write(*, *) 'Check'
  !       enddo
  !    else
  !       do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
  !          if(5401 <= matA%col_ind(i) .and. matA%col_ind(i) <= 5404) then
  !             write(*, *) j, matA%col_ind(i)
  !             if(j > 5405) write(*, *) 'Check'
  !          endif
  !       enddo
  !    endif
  ! enddo

  ! if(aflags%r_vec == 2) then
  !    write(*, *) 'Making right hand vector as matB \times randam vecotr'
  !    call read_mm_form(fname_B, Bn_row, Bn_val, matB%val, matB%row_ptr, matB%col_ind)
  !    allocate(temp_real(n_row))
  !    call random_number(temp_real)
  !    do i = 1, n_row
  !       ! x(i) = (temp_real(i), 0.0d0)
  !       x(i) = (1.0d0, 0.0d0)
  !    enddo
  !    deallocate(temp_real)
  !    avg = 0.0d0
  !    do i = 1, n_row
  !       avg = avg + real(x(i))
  !    enddo
  !    avg = avg / n_row
  !    do i = 1, n_row
  !       if(real(x(i))>avg) then
  !          x(i) = 1.0d0
  !       else
  !          x(i) = -1.0d0
  !       endif
  !    enddo
  !    b = 0.0d0
  !    do j = 1, n_row
  !       do i = matB%row_ptr(j), matB%row_ptr(j+1)-1
  !          b(matB%col_ind(i)) = b(matB%col_ind(i)) + matB%val(i) * x(matB%col_ind(i))
  !       enddo
  !    enddo
  !    x = 0.0d0
  ! else
  !    write(*, *) 'Making right hand vector as answer is 1'
  !    b = 0.0d0
  !    do j = 1, n_row
  !       do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
  !          b(j) = b(j) + matA%val(i)
  !       enddo
  !    enddo
  !    x = 0.0d0
  ! endif

  ! allocate(r(n_row))
  ! call check_sol(matA, b, x, n_row)
  ! call direct_para(matA, b, x, n_row)
  ! call calc_residual_full(matA, x, b, r, n_row)
  ! write(*, *) 'Res_direct', dot_product(r, r)
  ! stop

  ! do i = 1, n_row
  !    write(*, *) i, b(i)
  ! enddo
  
  ! call I_Cholesky(matA ,IC ,n_row)
  ! call ICCG(matA ,IC ,B ,x ,n_row ,100000 ,1.0d-7)

  ! call CG(matA ,B ,x ,n_row ,100000 ,1.0d-7)

  ! if(shift /= 0.0d0) write(*, *) 'Shifting with method', aflags%shift_method

  ! write(*, *) aflags%solver
  
  !From here
  if(me_proc == 1) then
     call M_para_Block_I_Cholesky(matA ,B_IC ,n_row, aflags, abmc, status_ord)
     ! call M_Block_I_Cholesky(matA ,B_IC ,n_row, aflags)
  endif
  call MPI_Bcast(status_ord, 1, MPI_INTEGER, 0, MCW, ierr)
  if(status_ord /= 0) then
     if(me_proc == 1) write(*, *) 'Program stop'
     call MPI_Finalize(ierr)
     stop
  endif
  ! call init_bound(n_row, num_proc, me_proc, abmc%row_start, abmc%row_end)
  call MPI_Bcast(B_IC%n_belem_row_glb, 1, MPI_Integer, 0, MCW, ierr)
  call MPI_Bcast(B_IC%padding, 1, MPI_Integer, 0, MCW, ierr)
  B_IC%bsize_row = aflags%bsize
  B_IC%bsize_col = aflags%bsize

  allocate(rrange%row_start(num_proc), rrange%row_end(num_proc), rrange%row_bstart(num_proc), rrange%row_bend(num_proc))
  do i = 1, num_proc
     call init_bound(B_IC%n_belem_row_glb, num_proc, i, rrange%row_bstart(i), rrange%row_bend(i))
     rrange%row_start(i) = (rrange%row_bstart(i)-1) * B_IC%bsize_row + 1
     rrange%row_end(i)   = rrange%row_bend(i) * B_IC%bsize_row
     if(i == num_proc) rrange%row_end(i) = n_row
  enddo
  ! call init_bound(B_IC%n_belem_row_glb, num_proc, me_proc, abmc%row_bstart, abmc%row_bend)
  abmc%row_bstart = rrange%row_bstart(me_proc)
  abmc%row_bend = rrange%row_bend(me_proc)
  abmc%row_start = rrange%row_start(me_proc)
  abmc%row_end = rrange%row_end(me_proc)

  B_IC%n_belem_row = abmc%row_bend - abmc%row_bstart + 1
  ! abmc%row_start = (abmc%row_bstart-1) * B_IC%bsize_row + 1
  ! abmc%row_end   = abmc%row_bend * B_IC%bsize_row
  matA%n_row = abmc%row_end - abmc%row_start + 1
  matA%n_row_glb = n_row
  write(*, *) 'Check range', me_proc, abmc%row_start, abmc%row_end, abmc%row_bstart, abmc%row_bend, matA%n_row

  call communicate_ordering(abmc, B_IC%n_belem_row_glb)
  ! call MPI_Barrier(MCW, ierr)
  ! if(me_proc == 1) write(*, *) 'Fin order'
  ! if(me_proc == 1) write(*, *) 'matArowptr', matA%row_ptr(129:190)
  call communicate_datas(matA, B_IC, n_row, abmc, rrange)
  !  call MPI_Barrier(MCW, ierr)
  ! if(me_proc == 1) write(*, *) 'Fin data'

  !To here
  ! write(*, *) 'Check', n_row, matA%n_row, abmc%row_end, abmc%row_start

  if(aflags%r_vec == 2) then
     write(*, *) 'Making right hand vector as matB \times randam vecotr'
     call read_mm_form(fname_B, Bn_row, Bn_val, matB%val, matB%row_ptr, matB%col_ind)
     allocate(temp_real(n_row))
     call random_number(temp_real)
     do i = 1, n_row
        ! x(i) = (temp_real(i), 0.0d0)
        x(i) = (1.0d0, 0.0d0)
     enddo
     deallocate(temp_real)
     avg = 0.0d0
     do i = 1, n_row
        avg = avg + real(x(i))
     enddo
     avg = avg / n_row
     do i = 1, n_row
        if(real(x(i))>avg) then
           x(i) = 1.0d0
        else
           x(i) = -1.0d0
        endif
     enddo
     b = 0.0d0
     do j = 1, n_row
        do i = matB%row_ptr(j), matB%row_ptr(j+1)-1
           b(matB%col_ind(i)) = b(matB%col_ind(i)) + matB%val(i) * x(matB%col_ind(i))
        enddo
     enddo
     x = 0.0d0
  else if(aflags%r_vec == 3) then
     if(me_proc == 1) then
        n_len = len_trim(fname_B)
        write(*, *) 'Read right hand vector from file', fname_B(1:n_len)
        call read_vec(fname_B, n_row, b)
     endif
     call MPI_Bcast(b, n_row, MPI_DOUBLE_PRECISION, 0, MCW, ierr)
  else
     if(me_proc == 1) write(*, *) 'Making right hand vector as answer is 1'
     b = 0.0d0
     do j = 1, matA%n_row
        do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
           b(j+abmc%row_start-1) = b(j+abmc%row_start-1) + matA%val(i)
        enddo
     enddo
  endif
  ! b(1) = 1.0d0
  ! b(3) = 1.0d0
  x = 0.0d0

  call make_mpi_struct_matA(abmc, matA, n_row, rrange)
  call make_mpi_struct_BIC(abmc, B_IC, B_IC%n_belem_row_glb)
  ! write(*, *) 'here'
  call MPI_Barrier(MCW, ierr)
  st_time = omp_get_wtime()
  call M_BICCG(matA ,B_IC ,B ,x ,n_row ,abmc, n_row, 1.0d-7, aflags)
  call MPI_Barrier(MCW, ierr)
  ed_time = omp_get_wtime()

  if(me_proc == 1) write(*, *) 'Time for solve is ', ed_time - st_time
  
  ! call ICCGA1(row_ptr ,col_ind, val, b, n_row, n_val, x, 100000, 1.0d-7, 1.0d0)

  call MPI_Finalize(ierr)
  
end program main

subroutine read_mm_form(fname, n_row, n_val, val, row_ptr, col_ind)
  implicit none
  integer, intent(out) :: n_row, n_val
  integer, allocatable, intent(inout) :: row_ptr(:), col_ind(:)
  double precision, allocatable, intent(inout) :: val(:)
  character(1000), intent(in) :: fname
  integer h, i, j, s_ind, e_ind
  integer n_col, row_ind, temp
  integer, allocatable :: row_temp(:)
  character(10000) :: oneline
  real(8) p_real, p_img
  integer :: fi = 10
  logical flag

  interface
     recursive subroutine quicksort(master, slave, val, n)
       implicit none
       integer, intent(in) :: n
       integer, intent(inout) :: master(:), slave(:)
       double precision, intent(inout) :: val(:)
     end subroutine quicksort
  end interface
  
  open(fi, file = fname)
  do while(.true.)
     read(fi, '(a)') oneline
     if(oneline(1:1) /= '%') exit
  enddo

  read(oneline, *) n_row, n_col, n_val
  allocate(row_ptr(n_row+1), col_ind(n_val), val(n_val), row_temp(n_val))

  do i = 1, n_val
     read(fi, *) row_temp(i), col_ind(i), val(i)
  enddo

  call quicksort(row_temp, col_ind, val, n_val)

  ! do i = 1, n_val 
  !    write(*, *) row_temp(i), col_ind(i), val(i)
  ! enddo

  row_ind = 1
  row_ptr(1) = 1
  j = 2
  do i = 1, n_val
     if(row_temp(i) /= row_ind) then
        row_ptr(j) = i
        row_ind = row_temp(i)
        j = j + 1
     endif
  enddo
  row_ptr(n_row + 1) = n_val + 1

  do i = 1, n_row
     s_ind = row_ptr(i)
     e_ind = row_ptr(i+1)-1
     ! if(i == 109) write(*, *) 'From here'
     call quicksort(col_ind(s_ind:e_ind), row_temp(s_ind:e_ind), val(s_ind:e_ind), e_ind - s_ind + 1)
     ! if(i == 109) write(*, *) 'To here'
     do h = row_ptr(i), row_ptr(i+1)-1
        if(col_ind(h) == i) exit
     enddo
     if(h == row_ptr(i+1)) then
        write(*, *) 'There are no diagonal entries'
        stop
     endif
  enddo
  
  ! row_ind = 1
  ! row_ptr(1) = 1
  ! j = 2
  ! do i = 1, n_val
  !    read(fi, *) temp, col_ind(i), val(i)
  !    !write(*, *) temp, col_ind(i), val(i)
  !    if(temp /= row_ind) then
  !       row_ptr(j) = i
  !       j = j + 1
  !       row_ind = temp
  !    endif
  ! enddo
  ! row_ptr(n_row+1) = n_val + 1
  
  close(fi)

   ! write(*, *) 'Check'
   ! write(*, *) row_ptr
   ! write(*, *)
   ! do i = 1, n_val 
   !    write(*, *) row_temp(i), col_ind(i), val(i)
   ! enddo
  
end subroutine read_mm_form
  
subroutine read_vec(fname, n_row, b)
  integer, intent(in) :: n_row
  double precision, allocatable, intent(inout) :: b(:)
  character(1000), intent(in) :: fname
  integer i, dummy
  integer :: fi = 11
  character(10000) :: oneline
  
  open(fi, file=fname)
  do while(.true.)
     read(fi, '(a)') oneline
     if(oneline(1:1) /= '%') exit
  enddo

  do i = 1, n_row
     read(fi, *) dummy, b(i)
  enddo
  close(fi)
  
end subroutine read_vec
  
recursive subroutine quicksort(master, slave, val, n)
  implicit none
  integer, intent(in) :: n
  integer, intent(inout) :: master(:), slave(:)
  double precision, intent(inout) :: val(:)
  integer i, j, pivot, temp_i
  double precision temp_r
  logical flag

  flag = .false.
  temp_i = master(1)
  do i = 2, n
     if(temp_i /= master(i)) then
        flag = .true.
        exit
     endif
  enddo

  if(.not. flag) return

  pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
  if(pivot == minval(master)) then
     do i = 1, n
        if(master(i) > pivot) then
           pivot = master(i)
           exit
        endif
     enddo
  endif

  i = 1
  j = n
  do while (i <= j)
     
     do while (master(i) < pivot .and. i < n)
        i = i + 1
     enddo

     do while (pivot <= master(j) .and. j > 1) 
        j = j - 1
     enddo

     if (i > j) exit

     temp_i = master(i)
     master(i) = master(j)
     master(j) = temp_i

     temp_i = slave(i)
     slave(i) = slave(j)
     slave(j) = temp_i

     temp_r = val(i)
     val(i) = val(j)
     val(j) = temp_r
     
     i = i + 1
     j = j - 1

  enddo

  !if(slave(1) == 109) then
  !   write(*, *) '###################################'
  !   write(*, *) 'pivot is ', pivot, n, i, j
  !   write(*, *) master
  !endif
  
  !write(*, *) 'Check after', i, j, n
  if (i-1 >= 2) call quicksort(master(1:i-1), slave(1:i-1), val(1:i-1), i-1)
  if (n - 1 + 1 >= 2) call quicksort(master(i:n), slave(i:n), val(i:n), n - i + 1) 
  
end subroutine quicksort

subroutine del_lower(matA, n_row, n_val)
  use coefficient
  implicit none
  type(CRS_mat), intent(inout) :: matA
  integer, intent(in) :: n_row
  integer, intent(inout) :: n_val
  integer, pointer :: n_col_ind(:)
  double precision, pointer :: na_val(:)
  integer, allocatable :: dummy(:)
  integer i, j, count, min, max, non_diag, count_d
  logical flag
  
  interface
     recursive subroutine quicksort(master, slave, val, n)
       implicit none
       integer, intent(in) :: n
       integer, intent(inout) :: master(:), slave(:)
       double precision, intent(inout) :: val(:)
     end subroutine quicksort
  end interface

  !If matrix dont have diagonal.
  non_diag = 0
  flag = .false.
  do j = 1, n_row
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        if(matA%col_ind(i) == j) flag = .true.
     enddo
     if(.not. flag) non_diag = non_diag + 1
     flag = .false.
  enddo
  
  n_val = (n_val-n_row+non_diag) / 2 + n_row
  write(*, *) 'Check size', n_val, n_row, non_diag
  allocate(n_col_ind(n_val), na_val(n_val), dummy(n_row))

  flag = .true.
  count = 1
  count_d = 1
  do j = 1, n_row
     min = count
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        if(matA%col_ind(i) >= j) then
           if(flag .and. matA%col_ind(i) /= j) then
              n_col_ind(count) = j
              na_val(count) = 0.0d0
              count = count + 1
              flag = .false.
           else
              n_col_ind(count) = matA%col_ind(i)
              na_val(count) = matA%val(i)
              count = count + 1
              flag = .false.
           endif
        else
           count_d = count_d + 1
        endif
     enddo
     max = count - 1
     matA%row_ptr(j) = min
     !write(*, *) min, max
     !if(max-min+1 >= 2) call quicksort(n_col_ind(min:max), dummy(1:max - min + 1), na_val(min:max), max - min + 1) 
     flag = .true.
  enddo
  matA%row_ptr(n_row+1) = n_val+1

  !write(*, *) 'Check fin', count, n_val, count-n_row, count_d
  if(count-n_row /= count_d) then
     write(*, *) 'This matrix is asymmetric.'
     stop
  endif
     
  ! deallocate(matA%val, matA%col_ind)
  matA%val => na_val
  matA%col_ind => n_col_ind
  
end subroutine del_lower

subroutine move_low_to_up(matA, n_row, n_val)
  use coefficient
  implicit none
  type(CRS_mat), intent(inout) :: matA
  integer, intent(in) :: n_row, n_val
  integer, pointer :: n_col_ind(:), n_row_ptr(:)
  double precision, pointer :: na_val(:)
  integer, allocatable :: a_count(:)
  integer i, j

  allocate(a_count(n_row), na_val(n_val), n_col_ind(n_val), n_row_ptr(n_row+1))
  a_count = 0

  do j = 1, n_row
     do i= matA%row_ptr(j), matA%row_ptr(j+1)-1
        a_count(matA%col_ind(i)) = a_count(matA%col_ind(i)) + 1
     enddo
  enddo

  n_row_ptr(1) = 1
  do i = 1, n_row
     n_row_ptr(i+1) = n_row_ptr(i) + a_count(i)
  enddo

  a_count = 0
  do j = 1, n_row
     do i= matA%row_ptr(j), matA%row_ptr(j+1)-1
        na_val(n_row_ptr(matA%col_ind(i)) + a_count(matA%col_ind(i))) = matA%val(i)
        n_col_ind(n_row_ptr(matA%col_ind(i)) + a_count(matA%col_ind(i))) = j
        a_count(matA%col_ind(i)) = a_count(matA%col_ind(i)) + 1
     enddo
  enddo

  ! deallocate(matA%val, matA%col_ind, matA%row_ptr)
  matA%val => na_val
  matA%col_ind => n_col_ind
  matA%row_ptr => n_row_ptr

  ! do j = 1, n_row
  !    do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
  !       write(*, *) j, matA%col_ind(i), matA%val(i)
  !    enddo
  ! enddo
  
end  subroutine move_low_to_up
