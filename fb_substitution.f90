!This is the forward-backward substitution routine.
!It is about the structure of the matrix, please show the module_iccg.f90.
!It is about the structure of the ordering(ord_para) and coloring-algorithm, please show the init_parallel.f90.
!There are the coloring algorithms in the init_parallel.f90.
!There is the imcomplete Cholesky decomposition routine in the ICCG_original_normalize.f90 

subroutine M_solveP_Block(B_IC, z, r, n, abmc, temp_a) !Forward-backward substitution part
  use coefficient
  use mod_parallel
  implicit none
  include 'mpif.h'
  integer, intent(in) :: n              !Number of non-zero elements
  double precision, intent(in) :: r(:)  !Right hand vector
  double precision, intent(out) :: z(:) !Solved vecotr
  type(BCRS_mat), intent(in) :: B_IC    !Block incomplete Cholesky
  type(ord_para), intent(in) :: abmc    !Ordering information
  type(temporary), intent(inout) :: temp_a !Temporary arrays for send and recv datas
  integer h, i, j, k, l, ind_row, id ,icolor, idx, proc
  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr, count
  integer, allocatable :: req(:), st_is(:, :)
  integer :: fo = 10, st_rv(MPI_STATUS_SIZE)
  double precision sum_temp
  character(100) :: fname
  
  call MPI_Comm_size(MCW ,num_proc ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1
  allocate(req(num_proc), st_is(MPI_STATUS_SIZE, num_proc))
  
  do icolor = 1, abmc%num_color !Forward substituion part

     count = 1 
     if(icolor /= 1) then !Communicate calculated vector. Send data from temp_a%sub to temp_a%sub_r
        do i = 1, num_proc
           if(i /= me_proc .and. abmc%color(icolor)%recv_count(i) /= 0) then
              call MPI_Isend(temp_a%sub, abmc%color(icolor)%recv_count(i), abmc%color(icolor)%recv_type(i), i-1, 0, MCW, req(count), ierr)
              count = count + 1
           endif
        enddo
        do i = 1, num_proc
           if(i /= me_proc .and. abmc%color(icolor)%send_count(i) /= 0) then
              call MPI_Recv(temp_a%sub_r(temp_a%ind_fwd(i, icolor)), &
                 abmc%color(icolor)%buf_send_tindex(i)%num*B_IC%bsize_row, MPI_Double_precision, i-1, 0, MCW, st_rv, ierr)
           endif
        enddo
     endif

     call MPI_Waitall(count-1, req, st_is, ierr)
   
     do proc = 1, num_proc !Apply recv data to temp_a%sub
        if(proc /= me_proc .and. abmc%color(icolor)%send_count(proc) /= 0) then
           do h = 1, abmc%color(icolor)%buf_send_tindex(proc)%num
              idx = abmc%color(icolor)%buf_send_tindex(proc)%disp(h)
              temp_a%sub(idx+1:idx+B_IC%bsize_row) = temp_a%sub(idx+1:idx+B_IC%bsize_row) + &
                 temp_a%sub_r(temp_a%ind_fwd(proc, icolor)+B_IC%bsize_row*(h-1):temp_a%ind_fwd(proc, icolor)+B_IC%bsize_row*h-1)
           enddo
        endif
     enddo

     do id = 1, abmc%color(icolor)%num_elems !Calculate part. Result is temp_a%sub
        j = abmc%color(icolor)%elem(id)      
        
        ind_row = (j-1) * B_IC%bsize_row + abmc%row_start !ind_row is the index of row.
        temp_a%zd(ind_row:ind_row+B_IC%bsize_row-1) = r(ind_row:ind_row+B_IC%bsize_row-1) - temp_a%sub(ind_row:ind_row+B_IC%bsize_row-1)
        do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
           temp_a%sub(B_IC%col_ind(i):B_IC%col_ind(i)+B_IC%bsize_col-1) = temp_a%sub(B_IC%col_ind(i):B_IC%col_ind(i)+B_IC%bsize_col-1) &
                                      + mat_vec_mult(transpose(B_IC%val(:, :, i)), temp_a%zd(ind_row:ind_row+B_IC%bsize_row-1), B_IC%bsize_row)
        enddo
        
     enddo

  enddo

  do icolor = abmc%num_color, 1, -1 !Backward substutuion part
     do id = abmc%color(icolor)%num_elems, 1, -1 !Calcuale part. Result is z.
        j = abmc%color(icolor)%elem(id)
        
        ind_row = (j-1) * B_IC%bsize_row + abmc%row_start
        temp_a%sub(1:B_IC%bsize_row) = 0.0d0
        do i = B_IC%row_ptr(j), B_IC%row_ptr(j+1)-1
           temp_a%sub(1:B_IC%bsize_row) = temp_a%sub(1:B_IC%bsize_row) &
                                         + mat_vec_mult(B_IC%val(:, :, i),  z(B_IC%col_ind(i):B_IC%col_ind(i)+B_IC%bsize_row-1), B_IC%bsize_row)
        enddo

        z(ind_row:ind_row+B_IC%bsize_row-1) = mat_vec_mult(B_IC%inv_dgn(:, :, j), temp_a%zd(ind_row:ind_row+B_IC%bsize_row-1), B_IC%bsize_row) &
                                                  - temp_a%sub(1:B_IC%bsize_row)

     enddo

     count = 1
     if(icolor /= 1) then !Cummunicate calculated data. Send from z to temp_a%sub_r
        do i = 1, num_proc
           if(i /= me_proc .and. abmc%color(icolor)%send_count(i) /= 0) then
              call MPI_Isend(z, abmc%color(icolor)%send_count(i), abmc%color(icolor)%send_type(i), i-1, 0, MCW, req(count), ierr)
              count = count + 1
           endif
        enddo
        do i = 1, num_proc
           if(i /= me_proc .and. abmc%color(icolor)%recv_count(i) /= 0) then
              call MPI_Recv(temp_a%sub_r(temp_a%ind_bck(i, icolor)), &
                 abmc%color(icolor)%buf_recv_tindex(i)%num*B_IC%bsize_row, MPI_Double_precision, i-1, 0, MCW, st_rv, ierr)
           endif
        enddo
     endif

     call MPI_Waitall(count-1, req, st_is, ierr)

     do proc = 1, num_proc !Input recv data temp_a%sub_r to z
        if(proc /= me_proc .and. abmc%color(icolor)%recv_count(proc) /= 0) then
           do h = 1, abmc%color(icolor)%buf_recv_tindex(proc)%num
              idx = abmc%color(icolor)%buf_recv_tindex(proc)%disp(h)
              z(idx+1:idx+B_IC%bsize_row) = &
                 temp_a%sub_r(temp_a%ind_bck(proc, icolor)+B_IC%bsize_row*(h-1):temp_a%ind_bck(proc, icolor)+B_IC%bsize_row*h-1)
           enddo
        endif
     enddo

  enddo

  temp_a%sub(abmc%row_start:abmc%row_end) = 0.0d0  !Initialize used part
  do icolor = 1, abmc%num_color
     do i = 1, num_proc
        if(i /= me_proc .and. abmc%color(icolor)%recv_count(i) /= 0) then
           do h = 1, abmc%color(icolor)%buf_recv_tindex(i)%num
              idx = abmc%color(icolor)%buf_recv_tindex(i)%disp(h)
              temp_a%sub(idx+1:idx+B_IC%bsize_row) = 0.0d0
           enddo
        endif
     enddo
  enddo

end subroutine M_solveP_Block
