module coefficient
#ifdef gnu
  use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI, POSV=>LA_POSV, SYGV=>LA_SYGV
#endif
#ifdef intel
  use lapack95, only : GETRF, GETRI, POSV, SYGV
#endif
  implicit none

  type CRS_mat !Structure for CRS format
     double precision, pointer :: val(:),  diag(:), inv_sqrt(:) !val:values, diag:Diagonal array, inv_sqrt:Inverse of the square-root of diagonal
     integer, pointer :: col_ind(:), row_ptr(:) !col_ind, row_ptr:column-index and row_ptr of the CRS format.
     integer n_row, n_row_glb !n_row:Number of rows in each processes. n_row_glb:Number of rows 
  end type CRS_mat

  type BCRS_mat !Structure for Block-CRS format
     integer  bsize_row, bsize_col, n_belem_row, padding, n_belem_row_glb !bsize_row:Size of block, bsize_col=bsize_row
              !n_belem_row:Number of block element row-direction (Local size of each process), n_belem_row_blg:n_belem_row of global
              !padding:Padding size Number of row elemets must be value * bsize
     double precision, pointer :: val(:, :, :), dgn(:, :, :), inv_dgn(:, :, :), inv_sqrt(:), inv_bsqrt(:, :, :)
              !val:Values with block-shape(row_index, column_index, ID)    This array is no include giagonal values
              !dgn:Values of diagonal-blocks, inv_dgn:inverse of the dgn
     integer, pointer :: col_ind(:), col_bind(:), row_ptr(:)
  end type BCRS_mat

  type CCS_mat_ind
     integer, pointer :: row_bind(:), col_ptr(:), rev_ind(:)
  end type CCS_mat_ind

  type temporary
     double precision, allocatable :: sub(:), sub_r(:), zd(:)
     integer, allocatable :: ind_fwd(:, :), ind_bck(:, :), ind_matvec(:)
  end type temporary

  interface
     function mat_vec_mult(mat, vec, size)
       integer, intent(in) :: size
       double precision, intent(in) :: mat(:, :), vec(:)
       double precision :: mat_vec_mult(size)
     end function mat_vec_mult
  end interface
  
end module coefficient
