module execarguments
  implicit none

  type argflags
     integer  read_type, solver, shift_method, normalize, r_vec, bsize, data_type, s_mod, color, grping, color_method
     real(8) shift_r
     complex((kind(0d0))) shift_c
     integer :: num_int = 11
  end type argflags
     
end module execarguments

subroutine read_args(fname_A, fname_B, aflags)
  use execarguments
  implicit none
  type(argflags), intent(out) :: aflags
  character(1000), intent(out) :: fname_A, fname_B
  integer count, n_len
  character(1000) :: read_strg, chs
  character(1) :: fopt
  real(8) temp_real, temp_img
  logical flag

  aflags%read_type = 1
  aflags%solver = 2
  aflags%shift_method = 0
  aflags%shift_r = 0.0d0
  aflags%shift_r = cmplx(0.0d0, 0.0d0)
  aflags%normalize = 0
  aflags%bsize = 4
  fname_b(1:1) = char(0)
  aflags%r_vec = 1
  aflags%data_type = 1
  aflags%s_mod = 0
  aflags%color_method = 2
  aflags%color = 1
  aflags%grping = 1
  
  ! write(*, *) command_argument_count()
  
  flag = .true.
  count = 1
  do while(count <= command_argument_count())
     ! write(*, *) 'Check1', count
     call get_command_argument(count, read_strg)   
     if(read_strg(1:1) == "-") then
        count = count + 1
        fopt = read_strg(2:2)
        call get_command_argument(count, read_strg)
        n_len = len_trim(read_strg)
        select case(fopt)
        case("t")
           select case(read_strg(1:n_len))
           case("MM")
              aflags%read_type = 1
           case("Bin")
              aflags%read_type = 2
           case default
              write(*, *) 'Choose the type of input data. MM (MatrixMarket) or Bin(Binary)'
              stop
           end select
        case("m")
           select case(read_strg(1:n_len))
           case("ICCG")
              aflags%solver = 1
           case("BICCG")
              aflags%solver = 2
           case("lapack")
              aflags%solver = 3
           case("pardiso")
              aflags%solver = 4
           case("original")
              aflags%solver = 5
           case("CG")
              aflags%solver = 6
           case default
              write(*, *) 'Choose the solve. ICCG, BICCG or Direct'
              stop
           end select
        case("s")
           select case(read_strg(1:n_len))
           case("none")
              aflags%shift_method = 0
           case("add")
              aflags%shift_method = 1
           case("mult")
              aflags%shift_method = 2
           case("offdiagonal")
              aflags%shift_method = 3
           case("offdiag-abs")
              aflags%shift_method = 4
           case("offdiag-avgblk")
              aflags%shift_method = 5
           case default
              write(*, *) 'Choose the method of diagonal shifting. add, mult or unitdiag'
              stop
           end select
           if(aflags%shift_method /= 0) then
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) aflags%shift_r
              ! count = count + 1
              ! call get_command_argument(count, read_strg)
              ! read(read_strg, *) temp_img
              ! aflags%shift_c = cmplx(temp_real, temp_img)
           endif
        case("n")
           select case(read_strg(1:n_len))
           case("none")
              aflags%normalize = 0
           case("unit")
              aflags%normalize = 1
           case("block")
              aflags%normalize = 2
           case default
              write(*, *) 'Choose the method of normalization. unit, block or none'
              stop
           end select
        case("b")
           read(read_strg, *) aflags%bsize
        case("r")
           select case(read_strg(1:n_len))
           case("unit")
              aflags%r_vec = 1
           case("matvec")
              aflags%r_vec = 2
           case("file")
              aflags%r_vec = 3
           case default
              write(*, *) 'Choose the method of making right hand vector. matvec or unit'
              stop
           end select
           if(aflags%r_vec /= 1) then
              count = count + 1
              call get_command_argument(count, fname_B)
              n_len = len_trim(fname_B)
              fname_B(n_len+1:n_len+1) = char(0)
           endif
        case("d")
           select case(read_strg(1:n_len))
           case("sym")
              aflags%data_type = 1
           case("asym")
              aflags%data_type = 2
           case default
              write(*, *) 'Choose the data type. sym or asym'
              stop
           end select
        case("c")
           select case(read_strg(1:n_len))
           case("amc")
              aflags%color_method = 1
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) aflags%color
           case("greedy")
              aflags%color_method = 2
           case("block_greedy")
              aflags%color_method = 3
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) aflags%grping
           case("block_greedy_reverse")
              aflags%color_method = 4
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) aflags%grping
           case("cm_amc")
              aflags%color_method = 5
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) aflags%color
           case("rcm_amc")
              aflags%color_method = 6
              count = count + 1
              call get_command_argument(count, read_strg)
              read(read_strg, *) aflags%color
           case("cm_greedy")
              aflags%color_method = 7
           case("rcm_greedy")
              aflags%color_method = 8
           case default
              write(*, *) 'Choose the method of coloring.'
              stop
           end select
        case default
           write(*, *) 'Option -', fopt, ' is invalid.'
           stop
        end select
     else
        if(flag) then
           read(read_strg, '(a)') fname_A
           n_len = len_trim(fname_A)
           fname_A(n_len+1:n_len+1) = char(0)
           flag = .false.
        else
           write(*, *) 'Arguments are invalid. Please check arguments.'
           write(*, *) read_strg(1:len_trim(read_strg))
           stop
        endif
     endif
     count = count + 1
     ! write(*, *) 'Check2', count
  enddo

  if(flag) then
     write(*, *) 'There is no input file.'
     stop
  endif
  if(aflags%r_vec /= 1 .and. fname_B(1:1) == char(0)) then
     write(*, *) 'There is no input file of matrix "B".'
     stop
  endif

  if(aflags%solver == 3 .or. aflags%solver == 4) then
     aflags%data_type = 2
  endif
  
  ! write(*, *) flag_t, flag_m, flag_s, shift, flag_n, bsize, fname, flag_r, fname_b
  
end subroutine read_args
