module mod_parallel

  type buf_mpistruct
     integer, allocatable :: disp(:), bsize(:)
     integer num
  end type buf_mpistruct

  type dlist
     integer, allocatable :: elem(:) !Elements included each color
     integer num_elems !Number of elements in each color
     integer, allocatable :: send_type(:), recv_type(:), send_disp(:), recv_disp(:), send_count(:), recv_count(:)
     type(buf_mpistruct), allocatable :: buf_send_tindex(:), buf_recv_tindex(:)
  end type dlist

  type ord_para
     type(dlist), allocatable :: color(:) !Structure dlist of each color
     type(buf_mpistruct), allocatable :: buf_send_tindex(:), buf_recv_tindex(:)
     integer, allocatable :: send_whole_type(:), recv_whole_type(:), send_disp(:), recv_disp(:), send_count(:), recv_count(:)
     integer num_color, row_start, row_end, row_bstart, row_bend
     !num_color:Number of colors
  end type ord_para

  type row_sepa
     integer, allocatable :: row_start(:), row_end(:), row_bstart(:), row_bend(:)
  end type row_sepa
  
end module mod_parallel

module metis_interface

  use iso_c_binding

  implicit none

  !If you use Metis, please copy below declaration commented out to your program and add a line "use metis_interface" before implicit declaration.
  ! integer(c_int) nvtxs, ncon, nparts
  ! integer(c_int), allocatable :: xadj(:), adjncy(:), objval(:), part(:)
  ! type(c_ptr) vwgt, vsize, adjwgt, tpwgts,  ubvec, opts
  ! integer(c_int) err !err is for return from metis. 1 indicates returned normally. -1 indicates an input error. &
                     !-2 indicates that it could not allocate the requred memory. -3 indicates some other type of error. Metis version is 5.1.0.

  interface
     integer(c_int) function METIS_PartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec &
                                               , opts, objval, part) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int),               intent(in)  :: nvtxs, ncon !nvtxs is the number of vetices. ncon is the number of balancing constraints.
       integer(c_int), dimension(*), intent(in)  :: xadj, adjncy !xadj is the vector of row pointer. adjncy is the adjancy list.
       type(c_ptr),                  value       :: vwgt !The weight of the vertices.
       type(c_ptr),                  value       :: vsize !The size of the vertices for computing the total communication volume.
       type(c_ptr),                  value       :: adjwgt !The weight of the edges.
       integer(c_int),               intent(in)  :: nparts !The number of parts to partition the graph.
       type(c_ptr),                  value       :: tpwgts !This specifies the desired weight for each partition and constraint.
       type(c_ptr),                  value       :: ubvec  !This specifies the allowed special tolerance for each constraint.
       type(c_ptr),                  value       :: opts !Defined value "METIS_NOPTIONS" in the metis.h is 40. Metis's version is 5.1.0.
       ! integer(c_int), dimension(*), intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int),               intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int), dimension(*), intent(out) :: part !This is a vector stores the patition vector of the graph.
     end function METIS_PartGraphKway

     integer(c_int) function METIS_PartGraphRecursive(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec &
                                               , opts, objval, part) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int),               intent(in)  :: nvtxs, ncon !nvtxs is the number of vetices. ncon is the number of balancing constraints.
       integer(c_int), dimension(*), intent(in)  :: xadj, adjncy !xadj is the vector of row pointer. adjncy is the adjancy list.
       type(c_ptr),                  value       :: vwgt !The weight of the vertices.
       type(c_ptr),                  value       :: vsize !The size of the vertices for computing the total communication volume.
       type(c_ptr),                  value       :: adjwgt !The weight of the edges.
       integer(c_int),               intent(in)  :: nparts !The number of parts to partition the graph.
       type(c_ptr),                  value       :: tpwgts !This specifies the desired weight for each partition and constraint.
       type(c_ptr),                  value       :: ubvec  !This specifies the allowed special tolerance for each constraint.
       type(c_ptr),                  value       :: opts !Defined value "METIS_NOPTIONS" in the metis.h is 40. Metis's version is 5.1.0.
       ! integer(c_int), dimension(*), intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int),               intent(out) :: objval !This variable stores edge-cut of patitioning solution.
       integer(c_int), dimension(*), intent(out) :: part !This is a vector stores the patition vector of the graph.
     end function METIS_PartGraphRecursive

     integer(c_int) function Metis_SetDefaultOptions(opts) bind(c)
       use, intrinsic :: iso_c_binding
       implicit none
       integer(c_int), dimension(0:39), intent(out)  :: opts
     end function Metis_SetDefaultOptions
  end interface

end module metis_interface

subroutine init_bound(n_row, num_proc, me_proc, min, max)
  use execarguments
  integer, intent(in) :: n_row, num_proc, me_proc
  integer, intent(out) :: min, max
  integer rem, num
  
  rem = mod(n_row, num_proc)
  num = (n_row-rem) / num_proc

  if(me_proc <= rem) then
     min = (me_proc-1) * (num + 1) + 1
     max = min + num 
  else
     min = (me_proc-1) * num + rem + 1
     max = min + num - 1
  endif
  
end subroutine init_bound

subroutine AMC_iwst(row_ptr, col_ind, amc, status)
  use mod_parallel
  implicit none
  integer, intent(in) :: row_ptr(:), col_ind(:)
  type(ord_para), intent(inout) :: amc
  integer, intent(out) :: status
  integer icolor, i, j, n_row, st_color
  integer, allocatable :: color_elem(:), num_elems_color(:)

  write(*, *) 'Algebraic multi-coloring', amc%num_color
  n_row = size(row_ptr)-1
  allocate(amc%color(amc%num_color), color_elem(row_ptr(n_row+1)), num_elems_color(amc%num_color))

  num_elems_color = 0
  color_elem = 0
  icolor = 1
  do j = n_row, 1, -1
     st_color = icolor

     i = row_ptr(j)
     do while(i <= row_ptr(j+1)-1)
        if(color_elem(col_ind(i)) == icolor) then
           icolor=mod(icolor, amc%num_color) + 1
           if(icolor == st_color) then
              write(*, *) 'Error. I can''t coloring all elements. Nubmer of color is less.'
              status = -1
              return
           endif
           i = row_ptr(j) - 1
        endif
        i = i + 1
     enddo
     color_elem(j) = icolor
     num_elems_color(icolor) = num_elems_color(icolor) + 1
     icolor=mod(icolor, amc%num_color) + 1
  enddo

  do i = 1, amc%num_color
     amc%color(i)%num_elems = num_elems_color(i)
     allocate(amc%color(i)%elem(num_elems_color(i)+1))
     amc%color(i)%elem(num_elems_color(i)+1) = n_row+1
  enddo

  num_elems_color = 1
  do i = 1, n_row
     amc%color(color_elem(i))%elem(num_elems_color(color_elem(i))) = i
     num_elems_color(color_elem(i)) = num_elems_color(color_elem(i)) + 1
  enddo

  status = 0

end subroutine AMC_iwst

subroutine CM_AMC_iwst(row_ptr, col_ind, amc, status)
  use mod_parallel
  implicit none
  integer, intent(in) :: row_ptr(:), col_ind(:)
  type(ord_para), intent(inout) :: amc
  integer, intent(out) :: status
  integer icolor, i, j, n_row, st_color, head, ptr_h, ptr_q, count, bsize
  integer, allocatable :: color_elem(:), num_elems_color(:), q_head(:)
  logical flag
  integer :: fo = 10
  
  write(*, *) 'CM - Algebraic multi-coloring', amc%num_color
  n_row = size(row_ptr)-1
  allocate(amc%color(amc%num_color), color_elem(n_row+1), num_elems_color(amc%num_color), q_head(n_row))
  
  num_elems_color = 0
  color_elem = 0
  icolor = 1

  icolor = 1
  ptr_h = 1
  ptr_q = 2
  q_head(1) = 1 !First head
  j = q_head(1)
  do while(.true.)
     st_color = icolor
     
     i = row_ptr(j)
     do while(i <= row_ptr(j+1)-1)
        if(color_elem(col_ind(i)) == icolor) then
           icolor=mod(icolor, amc%num_color) + 1
           if(icolor == st_color) then
              write(*, *) 'Error. I can''t coloring all elements. Nubmer of color is less.'
              status = -1
              return
           endif
           i = row_ptr(j) - 1
        endif
        i = i + 1
     enddo

     color_elem(j) = icolor
     num_elems_color(icolor) = num_elems_color(icolor) + 1
     icolor=mod(icolor, amc%num_color) + 1
     
     ! count = count + 1
     if(ptr_q > n_row) exit
     
     flag = .false.
     do while(.true.)
        head = q_head(ptr_h)
        do i = row_ptr(head), row_ptr(head+1)-1
           if(color_elem(col_ind(i)) == 0) then
              j = col_ind(i)
              flag = .true.
              exit
           endif
        enddo
        if(flag) exit
        ptr_h = ptr_h + 1
     enddo
     q_head(ptr_q) = j
     ptr_q = ptr_q + 1
     
  enddo

  do i = 1, n_row !This part is not needed at the final version. Checking routine has same safety.
     if(color_elem(i) == 0) write(*, *) 'Uncolored', i, color_elem(i)
  enddo

  do i = 1, amc%num_color
     amc%color(i)%num_elems = num_elems_color(i)
     allocate(amc%color(i)%elem(num_elems_color(i)+1))
     amc%color(i)%elem(num_elems_color(i)+1) = n_row+1 !For making communication arrays.
  enddo

  num_elems_color = 1
  do i = 1, n_row
     amc%color(color_elem(q_head(i)))%elem(num_elems_color(color_elem(q_head(i)))) = q_head(i)
     num_elems_color(color_elem(q_head(i))) = num_elems_color(color_elem(q_head(i))) + 1
  enddo
  
  status = 0
  
end subroutine CM_AMC_iwst

subroutine MC_Greedy(row_ptr, col_ind, amc, status)
  use mod_parallel
  use metis_interface
  implicit none
  integer, intent(in) :: row_ptr(:), col_ind(:)
  type(ord_para), intent(inout) :: amc
  integer, intent(out) :: status
  integer icolor, h, i, j, n_row, st_color
  integer, allocatable :: color_elem(:), num_elems_color(:)

  write(*, *) 'Multi-coloring with Greedy.'
  n_row = size(row_ptr)-1
  allocate(color_elem(n_row), num_elems_color(n_row))

  num_elems_color = 0
  color_elem = 0
  st_color = 1
  amc%num_color = 1
  do j = n_row, 1, -1

     icolor = 1
     i = row_ptr(j)
     do while(i <= row_ptr(j+1)-1)
        if(color_elem(col_ind(i)) == icolor) then
           icolor = icolor + 1
           i = row_ptr(j) - 1
        endif
        i = i + 1
     enddo

     color_elem(j) = icolor
     num_elems_color(icolor) = num_elems_color(icolor) + 1
     if(amc%num_color < icolor) amc%num_color = icolor
     
  enddo

  write(*, *) 'Number of color is', amc%num_color

  allocate(amc%color(amc%num_color))
  do i = 1, amc%num_color
     amc%color(i)%num_elems = num_elems_color(i)
     allocate(amc%color(i)%elem(num_elems_color(i)+1))
     amc%color(i)%elem(num_elems_color(i)+1) = n_row+1
  enddo

  num_elems_color = 1
  do i = 1, n_row
     amc%color(color_elem(i))%elem(num_elems_color(color_elem(i))) = i
     num_elems_color(color_elem(i)) = num_elems_color(color_elem(i)) + 1
  enddo

  status = 0  
  
end subroutine MC_Greedy

subroutine CM_MC_Greedy(row_ptr, col_ind, amc, status)
  use mod_parallel
  implicit none
  integer, intent(in) :: row_ptr(:), col_ind(:)
  type(ord_para), intent(inout) :: amc
  integer, intent(out) :: status
  integer icolor, i, j, n_row, st_color, head, ptr_h, ptr_q, count, max_ncol
  integer, allocatable :: color_elem(:), num_elems_color(:), q_head(:)
  logical flag
  integer :: fo = 10
  
  write(*, *) 'CM - Algebraic multi-coloring', amc%num_color
  n_row = size(row_ptr)-1
  allocate(color_elem(n_row+1), num_elems_color(n_row), q_head(n_row))
  
  num_elems_color = 0
  color_elem = 0
  amc%num_color = 1

  ptr_h = 1
  ptr_q = 2
  q_head(1) = 1 !First head
  j = q_head(1)
  do while(.true.)

     icolor = 1
     i = row_ptr(j)
     do while(i <= row_ptr(j+1)-1)
        if(color_elem(col_ind(i)) == icolor) then
           icolor= icolor + 1
           i = row_ptr(j) - 1
        endif
        i = i + 1
     enddo

     color_elem(j) = icolor
     num_elems_color(icolor) = num_elems_color(icolor) + 1
     if(amc%num_color < icolor) amc%num_color = icolor
     
     if(ptr_q > n_row) exit
     
     flag = .false.
     do while(.true.)
        head = q_head(ptr_h)
        do i = row_ptr(head), row_ptr(head+1)-1
           if(color_elem(col_ind(i)) == 0) then
              j = col_ind(i)
              flag = .true.
              exit
           endif
        enddo
        if(flag) exit
        ptr_h = ptr_h + 1
     enddo
     q_head(ptr_q) = j
     ptr_q = ptr_q + 1
     
  enddo

  do i = 1, n_row !This part is not needed at the final version. Checking routine has same safety.
     if(color_elem(i) == 0) write(*, *) 'Uncolored', i, color_elem(i)
  enddo

  write(*, *) 'Number of color is', amc%num_color
  allocate(amc%color(amc%num_color))
  do i = 1, amc%num_color
     amc%color(i)%num_elems = num_elems_color(i)
     allocate(amc%color(i)%elem(num_elems_color(i)+1))
     amc%color(i)%elem(num_elems_color(i)+1) = n_row+1 !For making communication arrays.
  enddo

  num_elems_color = 1
  do i = 1, n_row
     amc%color(color_elem(q_head(i)))%elem(num_elems_color(color_elem(q_head(i)))) = q_head(i)
     num_elems_color(color_elem(q_head(i))) = num_elems_color(color_elem(q_head(i))) + 1
  enddo

  status = 0
  
end subroutine CM_MC_Greedy

subroutine MC_metis_study(row_ptr, col_ind, amc, grping, status)
  use mod_parallel
  use metis_interface
  implicit none
  include 'mpif.h'
  integer, intent(in) :: row_ptr(:), col_ind(:)
  type(ord_para), intent(inout) :: amc
  integer, intent(in) :: grping
  integer, intent(out) :: status
  integer icolor, h, i, j, k, l, n_row, st_color, idx, jdx, col_max, proc, nparts
  integer, allocatable :: color_elem(:), num_elems_color(:), b_row_ptr(:), b_col_ind(:), b_id(:), temp_row_ptr(:), sub_part(:)
  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr
  integer :: fo = 10
  logical, allocatable :: check_all(:)
  
  integer(c_int) ncon, objval
  type(c_ptr) vwgt, vsize, adjwgt, tpwgts,  ubvec, opts
  integer(c_int) err
  integer(c_int), pointer :: sub_opts(:)
  type crs_ctype
     integer(c_int) nvtxs, nparts
     integer(c_int), allocatable :: xadj(:), adjncy(:), part(:)
     integer(c_int), pointer :: sub_vwgt(:)
     integer, allocatable :: col_st(:), col_ed(:)
     integer row_st, row_ed
  end type crs_ctype
  type(crs_ctype), allocatable :: sub_graph(:)

  write(*, *) 'Multicoloring with Greedy and metis.', grping
  call MPI_Comm_size(MCW ,num_proc ,ierr)
  n_row = size(row_ptr)-1

  allocate(sub_opts(40))
  allocate(sub_graph(num_proc))
  do proc = 1, num_proc
     call init_bound(n_row, num_proc, proc, sub_graph(proc)%row_st, sub_graph(proc)%row_ed)
     sub_graph(proc)%nvtxs = sub_graph(proc)%row_ed - sub_graph(proc)%row_st + 1
     allocate(sub_graph(proc)%xadj(sub_graph(proc)%nvtxs+1), sub_graph(proc)%part(sub_graph(proc)%nvtxs), &
        sub_graph(proc)%sub_vwgt(sub_graph(proc)%nvtxs), sub_graph(proc)%col_st(sub_graph(proc)%nvtxs), &
        sub_graph(proc)%col_ed(sub_graph(proc)%nvtxs))
     sub_graph(proc)%sub_vwgt = 1
  enddo

  err = Metis_SetDefaultOptions(sub_opts)
  if(err /= 1) then
     write(*, *) 'Metis library (METIS_SetDefaultOptions) return error. Error code is ', err
     stop
  endif
  sub_opts(2) = 0 !METIS_OBJETYPE=CUT
  sub_opts(18) = 1 !METIS_OPTIONS_NUMBERING=1 :: Fortran style
  opts = c_loc(sub_opts(1))
  ncon = 1

  do proc = 1, num_proc

     do j = sub_graph(proc)%row_st, sub_graph(proc)%row_ed
        jdx = j - sub_graph(proc)%row_st + 1

        sub_graph(proc)%col_st(jdx) = row_ptr(j+1)-1
        do i = row_ptr(j), row_ptr(j+1)-1
           if(sub_graph(proc)%row_st <= col_ind(i) .and. col_ind(i) <= sub_graph(proc)%row_ed) then
              sub_graph(proc)%col_st(jdx) = i
              exit
           endif
        enddo

        sub_graph(proc)%col_ed(jdx) = row_ptr(j+1)-2
        do i = row_ptr(j+1)-1, row_ptr(j), -1
           if(sub_graph(proc)%row_st <= col_ind(i) .and. col_ind(i) <= sub_graph(proc)%row_ed) then
              sub_graph(proc)%col_ed(jdx) = i
              exit
           endif
        enddo
     enddo

     allocate(sub_graph(proc)%adjncy(sum(sub_graph(proc)%col_ed) - sum(sub_graph(proc)%col_st) + sub_graph(proc)%nvtxs))
     sub_graph(proc)%xadj(1) = 1
     do i = 2, sub_graph(proc)%nvtxs+1
        sub_graph(proc)%xadj(i) = sub_graph(proc)%xadj(i-1) + sub_graph(proc)%col_ed(i-1) - sub_graph(proc)%col_st(i-1) + 1
        sub_graph(proc)%adjncy(sub_graph(proc)%xadj(i-1):sub_graph(proc)%xadj(i)-1) = col_ind(sub_graph(proc)%col_st(i-1):sub_graph(proc)%col_ed(i-1)) &
                                                                                                                          - sub_graph(proc)%row_st + 1
     enddo

     sub_graph(proc)%nparts = sub_graph(proc)%nvtxs / grping
     vsize = c_null_ptr
     adjwgt = c_null_ptr
     tpwgts = c_null_ptr
     ubvec = c_null_ptr
     vwgt = c_loc(sub_graph(proc)%sub_vwgt(1))

     write(*, *) 'Check number of partitioning', sub_graph(proc)%nparts, 'on proc', proc
     ! err = METIS_PartGraphKway(nvtxs, ncon, xadj, adjncy, vwgt, vsize, adjwgt, nparts, tpwgts, ubvec, opts, objval, part)
     err = METIS_PartGraphRecursive(sub_graph(proc)%nvtxs, ncon, sub_graph(proc)%xadj, sub_graph(proc)%adjncy, vwgt, vsize, adjwgt, sub_graph(proc)%nparts, tpwgts, ubvec, opts, objval, sub_graph(proc)%part)
     write(*, *) 'End PartGraph. Edge-cut volume of the patitioning solution is ', objval
     if(err /= 1) then
        write(*, *) 'Metis library (METIS_PartGraphKway) return error. Error code is ', err
        stop
     endif

  enddo

  allocate(sub_part(n_row))
  nparts = 0
  i = 0
  do proc = 1, num_proc
     sub_part(i+1:i+sub_graph(proc)%nvtxs) = sub_graph(proc)%part(1:sub_graph(proc)%nvtxs) + nparts
     nparts = nparts + sub_graph(proc)%nparts
     i = i + sub_graph(proc)%nvtxs
     deallocate(sub_graph(proc)%xadj, sub_graph(proc)%adjncy, sub_graph(proc)%sub_vwgt, sub_graph(proc)%part, &
                sub_graph(proc)%col_st, sub_graph(proc)%col_ed)
  enddo
  
  write(*, *) 'Number of nparts', nparts
  allocate(b_row_ptr(nparts+1), b_id(n_row), temp_row_ptr(nparts))

  temp_row_ptr = 0
  do i = 1, n_row
     temp_row_ptr(sub_part(i)) = temp_row_ptr(sub_part(i)) + 1
  enddo
  b_row_ptr(1) = 1
  do i = 2, nparts+1
     b_row_ptr(i) = b_row_ptr(i-1) + temp_row_ptr(i-1)
  enddo

  temp_row_ptr = 0
  do i = 1, n_row
     idx = sub_part(i)
     b_id(b_row_ptr(idx)+temp_row_ptr(idx)) = i
     temp_row_ptr(idx) = temp_row_ptr(idx) + 1
  enddo

  col_max = row_ptr(2) - row_ptr(1)
  do i = 2, n_row
     if(col_max < row_ptr(i+1) - row_ptr(i)) col_max = row_ptr(i+1) - row_ptr(i)
  enddo
  allocate(color_elem(n_row), num_elems_color(col_max*2))

  num_elems_color = 0
  color_elem = 0
  st_color = 1
  amc%num_color = 1
  do l = 1, nparts

     icolor = 1
     
     do k = b_row_ptr(l), b_row_ptr(l+1)-1 
        j = b_id(k)   
        i = row_ptr(j)
        do while(i <= row_ptr(j+1)-1)
           if(color_elem(col_ind(i)) == icolor) then
              icolor = icolor + 1
              if(amc%num_color < icolor) amc%num_color = icolor
              i = row_ptr(j) - 1
           endif
           i = i + 1
        enddo
     enddo

     do k = b_row_ptr(l), b_row_ptr(l+1)-1
        color_elem(b_id(k)) = icolor
     enddo
     num_elems_color(icolor) = num_elems_color(icolor) + b_row_ptr(l+1) - b_row_ptr(l)
     
  enddo
     
  allocate(amc%color(amc%num_color))
  do i = 1, amc%num_color
     amc%color(i)%num_elems = num_elems_color(i)
     allocate(amc%color(i)%elem(num_elems_color(i)+1))
     amc%color(i)%elem(num_elems_color(i)+1) = n_row+1 !This is for communicate ordering.
  enddo
  
  
  num_elems_color = 1
  do i = 1, n_row
     if(color_elem(i) == 0) then
        write(*, *) 'Check, uncolored element', i
        status = -1
        return
     endif
     amc%color(color_elem(i))%elem(num_elems_color(color_elem(i))) = i
     num_elems_color(color_elem(i)) = num_elems_color(color_elem(i)) + 1
  enddo
  
  write(*, *) 'Number of color is', amc%num_color
  write(*, *) 'Numbers of elements are', amc%color(:)%num_elems
  
  status = 0  

end subroutine MC_metis_study

subroutine communicate_datas(matA, B_IC, n_row, abmc, rrange)
  use coefficient
  use mod_parallel
  implicit none
  include 'mpif.h'
  type(CRS_mat), intent(inout) :: matA
  type(BCRS_mat), intent(inout) ::B_IC
  integer, intent(in) :: n_row
  type(ord_para), intent(inout) :: abmc
  type(row_sepa),intent(in) :: rrange
  ! integer, allocatable :: row_start(:), row_end(:)
  integer row_start, row_end, ptr_start, ptr_end, count_elem, row_bstart, row_bend, ptr_bstart, ptr_bend, count_belem
  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr
  integer :: st(MPI_STATUS_SIZE)
  integer i
  
  call MPI_Comm_size(MCW ,num_proc ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1

  if(me_proc == 1) then
     ! allocate(row_start(num_proc), row_end(num_proc))
     do i = 2, num_proc

        ! call init_bound(n_row, num_proc, i, row_start(i), row_end(i))
        call init_bound(B_IC%n_belem_row_glb, num_proc, i, row_bstart, row_bend)
        row_start = (row_bstart-1) * B_IC%bsize_row + 1
        if(i == num_proc) then
           row_end = matA%n_row_glb
        else
           row_end = row_bend * B_IC%bsize_row
        endif
        
        ptr_start = matA%row_ptr(row_start)
        ptr_end = matA%row_ptr(row_end+1)-1
        count_elem = ptr_end - ptr_start + 1

        ptr_bstart = B_IC%row_ptr(row_bstart)
        ptr_bend = B_IC%row_ptr(row_bend+1)-1
        count_belem = ptr_bend - ptr_bstart + 1
     
        ! if(i == 3) write(*, *) 'root', row_end-row_start+2
        call MPI_Send(matA%row_ptr(row_start:row_end+1), row_end-row_start+2, MPI_Integer, i-1, 0, MCW, ierr)
        ! if(i == 3) write(*, *) 'Send', matA%row_ptr(row_start:row_end+1)
        ! write(*, *) 'root, send1', count_elem, ptr_start, ptr_end
        call MPI_Send(matA%col_ind(ptr_start:ptr_end), count_elem, MPI_Integer, i-1, 1, MCW, ierr)
        ! write(*, *) 'root, send2'
        call MPI_Send(matA%val(ptr_start:ptr_end), count_elem, MPI_Double_precision, i-1, 2, MCW, ierr)
        
        ! write(*, *) 'root, send3'
        call MPI_Send(B_IC%row_ptr(row_bstart:row_bend+1), row_bend-row_bstart+2, MPI_Integer, i-1, 3, MCW, ierr)
        ! write(*, *) 'root, send4'
        call MPI_Send(B_IC%col_ind(ptr_bstart:ptr_bend), count_belem, MPI_Integer, i-1, 4, MCW, ierr)
        ! write(*, *) 'root, send5', count_belem, ptr_bstart, ptr_bend
        call MPI_Send(B_IC%col_bind(ptr_bstart:ptr_bend), count_belem, MPI_Integer, i-1, 5, MCW, ierr)
        ! write(*, *) 'root, send6'
        call MPI_Send(B_IC%val(1, 1, ptr_bstart), count_belem*B_IC%bsize_row**2, MPI_Double_precision, i-1, 6, MCW, ierr)
        ! write(*, *) 'root, send7'
        call MPI_Send(B_IC%dgn(1, 1, row_bstart), (row_bend-row_bstart+1)*B_IC%bsize_row**2, MPI_Double_precision, i-1, 7, MCW, ierr)
        ! write(*, *) 'root, send8'
        call MPI_Send(B_IC%inv_dgn(1, 1, row_bstart), (row_bend-row_bstart+1)*B_IC%bsize_row**2, MPI_Double_precision, i-1, 8, MCW, ierr)
       
     enddo

  else

     allocate(matA%row_ptr(matA%n_row+1))
     ! write(*, *) 'recv', me_proc, matA%n_row+1
     call MPI_Recv(matA%row_ptr, matA%n_row+1, MPI_Integer, 0, 0, MCW, st, ierr)
     ! if(me_proc == 3) write(*, *) 'before', matA%row_ptr
     do i = matA%n_row+1, 1, -1
        matA%row_ptr(i) = matA%row_ptr(i) - matA%row_ptr(1) + 1
     enddo
     ! if(me_proc == 3) write(*, *) 'after', matA%row_ptr
     count_elem = matA%row_ptr(matA%n_row+1) - 1
     allocate(matA%col_ind(count_elem), matA%val(count_elem))
     ! write(*, *) 'Chil, recv1', me_proc, count_elem
     call MPI_Recv(matA%col_ind, count_elem, MPI_Integer, i, 1, MCW, st, ierr)
     ! write(*, *) 'Chil, recv2', me_proc, count_elem
     call MPI_Recv(matA%val, count_elem, MPI_Double_precision, i, 2, MCW, st, ierr)

     
     allocate(B_IC%row_ptr(B_IC%n_belem_row+1))
     ! write(*, *) 'Chil, recv3', me_proc, B_IC%n_belem_row+1
     call MPI_Recv(B_IC%row_ptr, B_IC%n_belem_row+1, MPI_Integer, 0, 3, MCW, st, ierr)
     do i = B_IC%n_belem_row+1, 1, -1
        B_IC%row_ptr(i) = B_IC%row_ptr(i) - B_IC%row_ptr(1) + 1
     enddo
     count_belem = B_IC%row_ptr(B_IC%n_belem_row+1) - 1
     allocate(B_IC%col_ind(count_belem), B_IC%col_bind(count_belem), B_IC%val(B_IC%bsize_row, B_IC%bsize_row, count_belem))
     allocate(B_IC%dgn(B_IC%bsize_row, B_IC%bsize_row, B_IC%n_belem_row), B_IC%inv_dgn(B_IC%bsize_row, B_IC%bsize_row, B_IC%n_belem_row))
     ! write(*, *) 'Chil, recv4', me_proc, count_belem
     call MPI_Recv(B_IC%col_ind, count_belem, MPI_Integer, 0, 4, MCW, st, ierr)
     ! write(*, *) 'Chil, recv5', me_proc, count_belem
     call MPI_Recv(B_IC%col_bind, count_belem, MPI_Integer, 0, 5, MCW, st, ierr)
     ! write(*, *) 'Chil, recv6', me_proc, count_belem*B_IC%bsize_row**2
     call MPI_Recv(B_IC%val, count_belem*B_IC%bsize_row**2, MPI_Double_precision, 0, 6, MCW, st, ierr)
     ! write(*, *) 'Chil, recv7', me_proc, B_IC%n_belem_row*B_IC%bsize_row**2
     call MPI_Recv(B_IC%dgn, B_IC%n_belem_row*B_IC%bsize_row**2, MPI_Double_precision, 0, 7, MCW, st, ierr)
     ! write(*, *) 'Chil, recv8', me_proc, B_IC%n_belem_row*B_IC%bsize_row**2
     call MPI_Recv(B_IC%inv_dgn, B_IC%n_belem_row*B_IC%bsize_row**2, MPI_Double_precision, 0, 8, MCW, st, ierr)
   
  endif
   
end subroutine communicate_datas

subroutine communicate_ordering(abmc, n_row)
  use mod_parallel
  implicit none
  include 'mpif.h'
  type(ord_para), intent(inout) :: abmc
  integer, intent(in) :: n_row
  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr
  integer :: st(MPI_STATUS_SIZE)
  integer i, j, procm, num_elems_temp, count, proc, icolor, id, num_max
  integer, allocatable ::  list_temp(:, :), list_num(:), row_end(:)
  integer :: fo = 10
  character(100) :: fname
  
  call MPI_Comm_size(MCW ,num_proc ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1

  call MPI_Bcast(abmc%num_color, 1, MPI_Integer, 0, MCW, ierr)

  allocate(row_end(num_proc))
  do i = 1, num_proc
     call init_bound(n_row, num_proc, i, j, row_end(i))
  enddo

  if(me_proc == 1) then
     num_max = maxval(abmc%color(:)%num_elems)
     allocate(list_temp(num_max, num_proc), list_num(num_proc))
  else
     allocate(abmc%color(abmc%num_color))
  endif

  do icolor = 1, abmc%num_color
     
     if(me_proc == 1) then
        
        list_num = 0
        do id = 1, abmc%color(icolor)%num_elems
           j = abmc%color(icolor)%elem(id)
           proc = 1
           do while(.true.)
              if(row_end(proc) >= j .or. proc == num_proc) exit
              proc = proc + 1
           enddo           
           list_num(proc) = list_num(proc) + 1
           list_temp(list_num(proc), proc) = j
        enddo

        do proc = 2, num_proc
           call MPI_Send(list_num(proc), 1, MPI_Integer, proc-1, 0, MCW, ierr)
           if(list_num(proc) /= 0) call MPI_Send(list_temp(1:list_num(proc), proc), list_num(proc), MPI_Integer, proc-1, 0, MCW, ierr)
        enddo
        
        deallocate(abmc%color(icolor)%elem)
        abmc%color(icolor)%num_elems = list_num(1)
        allocate(abmc%color(icolor)%elem(list_num(1)))
        abmc%color(icolor)%elem(1:list_num(1)) = list_temp(1:list_num(1), 1)
        
     else

        call MPI_Recv(abmc%color(icolor)%num_elems, 1, MPI_Integer, 0, 0, MCW, st, ierr)
        allocate( abmc%color(icolor)%elem(abmc%color(icolor)%num_elems) )
        if(abmc%color(icolor)%num_elems /= 0) then
           call MPI_Recv(abmc%color(icolor)%elem, abmc%color(icolor)%num_elems, MPI_Integer, 0, 0, MCW, st, ierr)
        endif
        do i = 1, abmc%color(icolor)%num_elems
           abmc%color(icolor)%elem(i) = abmc%color(icolor)%elem(i) - abmc%row_bstart + 1
        enddo
     
     endif
     
  enddo

end subroutine communicate_ordering

subroutine make_mpi_struct_BIC(abmc, B_IC, n_row) !Makeing mpi data types for communicatons.
  use coefficient
  use mod_parallel
  implicit none
  include 'mpif.h'
  type list_temp
     integer num
     integer, allocatable :: list(:)
  end type list_temp
  type(ord_para), intent(inout) :: abmc
  type(BCRS_mat), intent(in) ::B_IC
  integer, intent(in) :: n_row
  integer i, j, k, l, icolor
  integer, allocatable :: row_end(:), buf_temp(:)
  logical, allocatable :: vec_logi(:)
  type(list_temp), allocatable :: buf_send(:), buf_recv(:)
  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr
  integer :: st(MPI_STATUS_SIZE)
  integer, allocatable :: req(:)
  integer, allocatable :: a_test(:)
  double precision, allocatable :: send_test(:), recv_test(:)
  
  call MPI_Comm_size(MCW ,num_proc ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1

  allocate(row_end(0:num_proc), buf_send(num_proc), buf_recv(num_proc))
  row_end(0) = 0
  do i = 1, num_proc
     call init_bound(n_row, num_proc, i, j, row_end(i))
  enddo

  allocate(vec_logi(n_row+1))
  vec_logi = .false.
  do i = 1, B_IC%row_ptr(B_IC%n_belem_row+1)-1
     vec_logi(B_IC%col_bind(i)) = .true.
  enddo

  call MPI_Barrier(MCW, ierr)

  buf_recv(:)%num = 0
  j = 1
  do i = 1, n_row+1 !Count and make list of request data.
     if(i > row_end(j)) then
        allocate(buf_recv(j)%list(buf_recv(j)%num))
        l = buf_recv(j)%num
        do k = i-1, row_end(j-1)+1, -1
           if(vec_logi(k)) then
              buf_recv(j)%list(l) = k
              l = l - 1
           endif
           if(l == 0) exit
        enddo
        j = j + 1
     endif
     if(vec_logi(i)) buf_recv(j)%num = buf_recv(j)%num + 1
  enddo

  call MPI_alltoall(buf_recv(:)%num, 1, MPI_Integer, buf_send(:)%num, 1, MPI_Integer, MCW, ierr) !Send number of request data.

  allocate(req(num_proc))
  do i = 1, num_proc !Send request list
     if(buf_recv(i)%num /= 0 .and. i /= me_proc) call MPI_iSend(buf_recv(i)%list, buf_recv(i)%num, MPI_Integer, i-1, 0, MCW, req(i), ierr)
  enddo

  do icolor = 1, abmc%num_color
     allocate(abmc%color(icolor)%send_type(num_proc), abmc%color(icolor)%send_disp(num_proc), abmc%color(icolor)%send_count(num_proc), &
              abmc%color(icolor)%recv_type(num_proc), abmc%color(icolor)%recv_disp(num_proc), abmc%color(icolor)%recv_count(num_proc), &
              abmc%color(icolor)%buf_send_tindex(num_proc), abmc%color(icolor)%buf_recv_tindex(num_proc))
  enddo

  !Communicate sending data of each color and MPI_struct type.
  do i = 1, num_proc

     if(buf_send(i)%num /= 0 .and. i /= me_proc) then

        allocate(buf_send(i)%list(buf_send(i)%num))
        call MPI_recv(buf_send(i)%list, buf_send(i)%num, MPI_Integer, i-1, 0, MCW, st, ierr) !Recv request list from processs i
        allocate(buf_temp(buf_send(i)%num))

        allocate(a_test(buf_send(i)%num)) !For debug
        a_test = 0 !For debug

        do icolor = 1, abmc%num_color !Check the color-id of the requested data and making the mpi data-type
           
           abmc%color(icolor)%buf_send_tindex(i)%num = 0
           do j = 1, abmc%color(icolor)%num_elems
              
              k = 1
              do while (abmc%color(icolor)%elem(j) + abmc%row_bstart - 1 > buf_send(i)%list(k))
                 k = k + 1
                 if(k > buf_send(i)%num) then
                    k = buf_send(i)%num
                    exit
                 endif
              enddo

              if(abmc%color(icolor)%elem(j) + abmc%row_bstart - 1 == buf_send(i)%list(k)) then
                 abmc%color(icolor)%buf_send_tindex(i)%num = abmc%color(icolor)%buf_send_tindex(i)%num + 1
                 buf_temp(abmc%color(icolor)%buf_send_tindex(i)%num) = buf_send(i)%list(k)
                 a_test(k) = icolor  !For debug
              endif
              
           enddo

           allocate(abmc%color(icolor)%buf_send_tindex(i)%disp(abmc%color(icolor)%buf_send_tindex(i)%num), &
                    abmc%color(icolor)%buf_send_tindex(i)%bsize(abmc%color(icolor)%buf_send_tindex(i)%num))
           abmc%color(icolor)%buf_send_tindex(i)%bsize = B_IC%bsize_row
           do j = 1, abmc%color(icolor)%buf_send_tindex(i)%num
              abmc%color(icolor)%buf_send_tindex(i)%disp(j) = (buf_temp(j) - 1) * B_IC%bsize_row
           enddo

           call MPI_Isend(abmc%color(icolor)%buf_send_tindex(i)%num, 1, MPI_Integer, i-1, icolor, MCW, req, ierr)
           !Send the number of datas sendind at icolor in the forward-backward substituion.
           if(abmc%color(icolor)%buf_send_tindex(i)%num /= 0) then
              call MPI_Isend(abmc%color(icolor)%buf_send_tindex(i)%disp, abmc%color(icolor)%buf_send_tindex(i)%num, MPI_Integer, i-1, icolor*2, &
                             MCW, req, ierr)
             !Send array indices sendind at icolor in the forward-backward substituion.
              call MPI_Type_indexed(abmc%color(icolor)%buf_send_tindex(i)%num, abmc%color(icolor)%buf_send_tindex(i)%bsize, &
                                    abmc%color(icolor)%buf_send_tindex(i)%disp, MPI_Double_precision, abmc%color(icolor)%send_type(i), ierr)
              call MPI_Type_commit(abmc%color(icolor)%send_type(i), ierr)
              abmc%color(icolor)%send_disp(i) = 0
              abmc%color(icolor)%send_count(i) = 1
           else
              abmc%color(icolor)%send_type(i) = MPI_Double_precision
              abmc%color(icolor)%send_disp(i) = 0
              abmc%color(icolor)%send_count(i) = 0
           endif
           
        enddo
        
        deallocate(buf_temp)
        
        do j = 1, buf_send(i)%num  !For debug
           if(a_test(j) == 0) then
              write(*, *) 'Missed sending data', me_proc, buf_send(i)%list(j), j, i, buf_send(i)%list(j)-abmc%row_bstart+1
              write(*, *) 'Missed sending data info1', me_proc, buf_send(i)%num, buf_send(i)%list(1:buf_send(i)%num)-abmc%row_bstart+1
              write(*, *) 'Missed sending data info2', me_proc, a_test(1:buf_send(i)%num)
              stop
           endif
        enddo
        deallocate(a_test)

     else
        do j = 1, abmc%num_color
           abmc%color(j)%send_type(i) = MPI_Double_precision
           abmc%color(j)%send_disp(i) = 0
           abmc%color(j)%send_count(i) = 0
        enddo
     endif
     
  enddo
  
  do  i = 1, num_proc
     
     if(buf_recv(i)%num /= 0 .and. me_proc /= i) then

        do icolor = 1, abmc%num_color

           call MPI_Recv(abmc%color(icolor)%buf_recv_tindex(i)%num, 1, MPI_Integer, i-1, icolor, MCW, st, ierr)
           !Recv the number of datas reciving at icolor in the forward-backward substituion.
           
           allocate(abmc%color(icolor)%buf_recv_tindex(i)%disp(abmc%color(icolor)%buf_recv_tindex(i)%num), &
                    abmc%color(icolor)%buf_recv_tindex(i)%bsize(abmc%color(icolor)%buf_recv_tindex(i)%num))

           if(abmc%color(icolor)%buf_recv_tindex(i)%num /= 0) then
              call MPI_Recv(abmc%color(icolor)%buf_recv_tindex(i)%disp, abmc%color(icolor)%buf_recv_tindex(i)%num, MPI_Integer, i-1, icolor*2, &
                            MCW, st, ierr)
             !Recv array indices reciving at icolor in the forward-backward substituion.
              abmc%color(icolor)%buf_recv_tindex(i)%bsize = B_IC%bsize_row
              call MPI_Type_indexed(abmc%color(icolor)%buf_recv_tindex(i)%num, abmc%color(icolor)%buf_recv_tindex(i)%bsize, &
                                    abmc%color(icolor)%buf_recv_tindex(i)%disp, MPI_Double_precision, abmc%color(icolor)%recv_type(i), ierr)
              call MPI_Type_commit(abmc%color(icolor)%recv_type(i), ierr)
              abmc%color(icolor)%recv_disp(i) = 0
              abmc%color(icolor)%recv_count(i) = 1
           else
              abmc%color(icolor)%recv_type(i) = MPI_Double_precision
              abmc%color(icolor)%recv_disp(i) = 0
              abmc%color(icolor)%recv_count(i) = 0
           endif

        enddo

     else
        do j = 1, abmc%num_color
           abmc%color(j)%recv_type(i) = MPI_Double_precision
           abmc%color(j)%recv_disp(i) = 0
           abmc%color(j)%recv_count(i) = 0
        enddo
     endif
     
  enddo

  call MPI_Barrier(MCW, ierr)

end subroutine make_mpi_struct_BIC

subroutine make_mpi_struct_matA(abmc, matA, n_row, rrange)
  use coefficient
  use mod_parallel
  implicit none
  include 'mpif.h'
  type list_temp
     integer num
     integer, allocatable :: list(:)
  end type list_temp
  type(ord_para), intent(inout) :: abmc
  type(CRS_mat), intent(in) ::matA
  integer, intent(in) :: n_row
  type(row_sepa),intent(in) :: rrange
  integer i, j, k, l
  integer, allocatable :: row_end(:), buf_temp(:)
  logical, allocatable :: vec_logi(:)
  type(list_temp), allocatable :: buf_send(:), buf_recv(:)
  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr
  integer :: st(MPI_STATUS_SIZE)
  integer, allocatable :: req(:)
  integer, allocatable :: a_test(:)
  
  ! write(*, *) 'here-2'
  
  call MPI_Comm_size(MCW ,num_proc ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1

  allocate(row_end(0:num_proc), buf_send(num_proc), buf_recv(num_proc))
  row_end(0) = 0
  row_end(1:num_proc) = rrange%row_end(1:num_proc)

  allocate(vec_logi(n_row+1))
  vec_logi = .false.
  do i = 1, matA%row_ptr(matA%n_row+1)-1
     vec_logi(matA%col_ind(i)) = .true.
  enddo

  buf_recv(:)%num = 0
  j = 1
  do i = 1,  n_row+1 !Count and make list of request datas
     if(i > row_end(j)) then
        allocate(buf_recv(j)%list(buf_recv(j)%num))
        l = buf_recv(j)%num
        do k = i-1, row_end(j-1)+1, -1
           if(vec_logi(k)) then
              buf_recv(j)%list(l) = k
              l = l - 1
           endif
           if(l == 0) exit
        enddo
        j = j + 1
     endif
     if(vec_logi(i)) buf_recv(j)%num = buf_recv(j)%num + 1
  enddo

  call MPI_alltoall(buf_recv(:)%num, 1, MPI_Integer, buf_send(:)%num, 1, MPI_Integer, MCW, ierr)

  call MPI_Barrier(MCW, ierr)

  allocate(req(num_proc))
  do i = 1, num_proc
     if(buf_recv(i)%num /= 0 .and. i /= me_proc) call MPI_iSend(buf_recv(i)%list, buf_recv(i)%num, MPI_Integer, i-1, 0, MCW, req(i), ierr)
  enddo

  allocate(abmc%send_whole_type(num_proc), abmc%recv_whole_type(num_proc), abmc%send_disp(num_proc) &
         , abmc%recv_disp(num_proc), abmc%send_count(num_proc), abmc%recv_count(num_proc) &
         , abmc%buf_send_tindex(num_proc), abmc%buf_recv_tindex(num_proc))

  do i = 1, num_proc
     if(i /= me_proc .and. buf_recv(i)%num /= 0) then
        abmc%buf_recv_tindex(i)%num = buf_recv(i)%num
        allocate(abmc%buf_recv_tindex(i)%disp(abmc%buf_recv_tindex(i)%num), abmc%buf_recv_tindex(i)%bsize(abmc%buf_recv_tindex(i)%num))
        do j = 1, abmc%buf_recv_tindex(i)%num
           abmc%buf_recv_tindex(i)%disp(j) = buf_recv(i)%list(j) - 1
        enddo
        abmc%buf_recv_tindex(i)%bsize = 1

        call MPI_Type_indexed(abmc%buf_recv_tindex(i)%num, abmc%buf_recv_tindex(i)%bsize, abmc%buf_recv_tindex(i)%disp, MPI_Double_precision &
                            , abmc%recv_whole_type(i), ierr)
        call MPI_Type_commit(abmc%recv_whole_type(i), ierr)
        abmc%recv_disp(i) = 0
        abmc%recv_count(i) = 1
     else
        abmc%recv_whole_type(i) = MPI_Double_precision
        abmc%recv_disp(i) = 0
        abmc%recv_count(i) = 0        
        abmc%buf_recv_tindex(i)%num = 0
     endif
  enddo
  
  do i = 1, num_proc
     if(i /= me_proc .and. buf_send(i)%num /= 0) then
        abmc%buf_send_tindex(i)%num = buf_send(i)%num
        allocate(abmc%buf_send_tindex(i)%disp(buf_send(i)%num), abmc%buf_send_tindex(i)%bsize(buf_send(i)%num))
        call MPI_Recv(abmc%buf_send_tindex(i)%disp, abmc%buf_send_tindex(i)%num, MPI_Integer, i-1, 0, MCW, st, ierr)
        do j = 1, abmc%buf_send_tindex(i)%num
           abmc%buf_send_tindex(i)%disp(j) = abmc%buf_send_tindex(i)%disp(j) - 1
        enddo
        abmc%buf_send_tindex(i)%bsize = 1
        call MPI_Type_indexed(abmc%buf_send_tindex(i)%num, abmc%buf_send_tindex(i)%bsize, abmc%buf_send_tindex(i)%disp, MPI_Double_precision &
                            , abmc%send_whole_type(i), ierr)
        call MPI_Type_commit(abmc%send_whole_type(i), ierr)
        abmc%send_disp(i) = 0
        abmc%send_count(i) = 1
     else
        abmc%send_whole_type(i) = MPI_Double_precision
        abmc%send_disp(i) = 0
        abmc%send_count(i) = 0
        abmc%buf_send_tindex(i)%num = 0
     endif
  enddo

  call MPI_Barrier(MCW, ierr)
  
end subroutine make_mpi_struct_matA

subroutine expand_up_to_low(row_ptr, col_ind, exp_row_ptr, exp_col_ind)
  implicit none
  integer, intent(in) :: row_ptr(:), col_ind(:)
  integer, allocatable, intent(out) :: exp_row_ptr(:), exp_col_ind(:)
  integer i, j, k, n_row
  integer, allocatable :: count_elem(:)

  n_row = size(row_ptr)-1

  allocate(count_elem(n_row))
  count_elem = 0
  do j = 1, n_row
     count_elem(j) = count_elem(j) + row_ptr(j+1) - row_ptr(j)
     do i = row_ptr(j), row_ptr(j+1)-1
        count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
     enddo
  enddo

  allocate(exp_row_ptr(n_row+1), exp_col_ind((row_ptr(n_row+1)-1)*2))
  exp_row_ptr(1) = 1
  do i = 2, n_row+1
     exp_row_ptr(i) = exp_row_ptr(i-1) + count_elem(i-1)
     count_elem(i-1) = 0
  enddo

  do j = 1, n_row
     k = row_ptr(j+1)-row_ptr(j)
     exp_col_ind(exp_row_ptr(j+1)-k:exp_row_ptr(j+1)-1) = col_ind(row_ptr(j):row_ptr(j+1)-1)
     do i = row_ptr(j), row_ptr(j+1)-1
        exp_col_ind(exp_row_ptr(col_ind(i))+count_elem(col_ind(i))) = j
        count_elem(col_ind(i)) = count_elem(col_ind(i)) + 1
     enddo
  enddo
        
end subroutine expand_up_to_low

subroutine reverse_order(amc)
  use mod_parallel
  implicit none
  type(ord_para), intent(inout) :: amc
  integer i, j, icolor
  type(ord_para) :: dummy

  write(*, *) 'Reverse ordering'
  
  allocate(dummy%color(amc%num_color))
  do icolor = 1, amc%num_color
     dummy%color(icolor)%num_elems = amc%color(icolor)%num_elems
     allocate(dummy%color(icolor)%elem(amc%color(icolor)%num_elems+1))
     dummy%color(icolor)%elem = amc%color(icolor)%elem
     deallocate(amc%color(icolor)%elem)
  enddo

  do icolor = amc%num_color, 1, -1
     amc%color(icolor)%num_elems = dummy%color(amc%num_color-icolor+1)%num_elems
     allocate(amc%color(icolor)%elem(amc%color(icolor)%num_elems+1))
     amc%color(icolor)%elem = dummy%color(amc%num_color-icolor+1)%elem
     deallocate(dummy%color(amc%num_color-icolor+1)%elem)
  enddo

  deallocate(dummy%color)

end subroutine reverse_order
