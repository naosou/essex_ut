As for a forward-backward substitution, please show the fb_substitution.f90
Introducing the other files.
ICCG_original_normalize.f90 : IC decomposition, CG
init_parallel.f90 : Making_mpi_type_structs, Communicating_data_for_initialization
read_args.f90 : Reading_run_time_arguments
module_iccg.f90 : Structure_declarations
main.f90 : Main_routine

This program needs metis and lapack95.
Make file is not enough generic.
Please modify Makefile  or add library in the working directory to linking libmetis and libpakack95
Only iterating part is parallelized. The imcomplete docompose and coloring parts are not parallelized.

Example to exec my program.

mpirun -np [Number_of_process] ./block_ICCG_mpi [input_file(MM format)] -m BICCG -b [size_of_block] -c [Coloring_method] -s [Diagonal handling method]

   -m : choose method   (defaults BICCG)
	-m BICCG  :Block ICCG is only work, now. 

   -b : Size of block   (defaults 4)
        -b 4  :Size of block is 4

   -c : Choosing coloring mehod (defaults amc 10)
        -c amc 10  :Algebraic multi-coloring with 10 colors

   -s : Choosing Diagonal handling method  (defaults none)
        -s none        :Any handling to the diagonal elements
        -s add 1.0d0   :Diagonal elements + 1.0d0
        -s mult 1.3d0  :Diagonal elements * 1.3d0

exmple
mpirun -np 4 ./block_ICCG_mpi graph-1k.mm -m BICCG -b 8 -c amc 20 -s add 100.0d0
