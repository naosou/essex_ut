subroutine M_para_Block_I_Cholesky(matA, B_IC, n, aflags, abmc, status_ord)
  use coefficient
  use execarguments
  use mod_parallel
  implicit none
  integer, intent(in) :: n
  type(CRS_mat), intent(in) :: matA
  type(BCRS_mat), intent(out) :: B_IC
  type(argflags), intent(in) :: aflags
  type(ord_para), intent(inout) :: abmc
  integer, intent(inout) :: status_ord
  integer, allocatable :: row_ptr_U(:), ind_to_bid(:),temp_col(:), pivot(:), reod(:), row_count(:), count_col(:)
  integer, allocatable :: exp_row_ptr(:), exp_col_bind(:)
  integer h, i, j, k, n_block, col_ptr, max_block_col, max_block_row, bind_row, ind_col, bind_col, n_val, bsize_col
  integer inb_ind_col, inb_ind_row, bsize, count, info, bsize_row, temp_i, icolor, kk, s_ind, e_ind, l, m, n_belem_max
  double precision w, v
  logical flag
  logical, allocatable :: logic_row(:)
  type(CCS_mat_ind) :: BCCS
  type(BCRS_mat) :: bmatA_back
  double precision, allocatable :: temp_mat(:, :, :), temp_L(:, :), eigv(:), temp_smat(:, :)
  real(8), allocatable :: sum_off(:)
  real(8) min_r, max_r, sum_r
  integer :: fo = 10

  interface

     subroutine AMC_iwst(row_ptr, col_ind, amc, status)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_ptr(:), col_ind(:)
       type(ord_para), intent(inout) :: amc
       integer, intent(out) :: status
     end subroutine AMC_iwst
     
     subroutine MC_greedy(row_ptr, col_ind, amc, status)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_ptr(:), col_ind(:)
       type(ord_para), intent(inout) :: amc
       integer, intent(out) :: status
     end subroutine MC_greedy

     subroutine MC_metis_study(row_ptr, col_ind, amc, grping, status)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_ptr(:), col_ind(:)
       type(ord_para), intent(inout) :: amc
       integer, intent(in) :: grping
       integer, intent(out) :: status
     end subroutine MC_metis_study

     subroutine CM_AMC_iwst(row_ptr, col_ind, amc, status)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_ptr(:), col_ind(:)
       type(ord_para), intent(inout) :: amc
       integer, intent(out) :: status
     end subroutine CM_AMC_iwst

     subroutine CM_MC_greedy(row_ptr, col_ind, amc, status)
       use mod_parallel
       implicit none
       integer, intent(in) :: row_ptr(:), col_ind(:)
       type(ord_para), intent(inout) :: amc
       integer, intent(out) :: status
     end subroutine CM_MC_greedy
     recursive subroutine quicksort_blk(master, slave, val, n, size_row, size_col)
       implicit none
       integer, intent(in) :: n, size_row, size_col
       integer, intent(inout) :: master(:), slave(:)
       double precision, intent(inout) :: val(:, :, :)
     end subroutine quicksort_blk
     
     subroutine expand_up_to_low(row_ptr, col_ind, exp_row_ptr, exp_col_ind)
       implicit none
       integer, intent(in) :: row_ptr(:), col_ind(:)
       integer, allocatable, intent(out) :: exp_row_ptr(:), exp_col_ind(:)
     end subroutine expand_up_to_low
     
  end interface
    
  bsize_row = aflags%bsize
  B_IC%bsize_row = bsize_row
  B_IC%bsize_col = bsize_row
  bsize_col = bsize_row
  if(mod(n, bsize_row) == 0) then
     B_IC%padding = 0
  else
     B_IC%padding = bsize_row - mod(n, bsize_row)
  endif
  if(mod(n, bsize_col) == 0) then
     max_block_col = (n - mod(n, bsize_col)) / bsize_col
     max_block_row = (n - mod(n, bsize_row)) / bsize_row
  else
     max_block_col = (n - mod(n, bsize_col)) / bsize_col + 1
     max_block_row = (n - mod(n, bsize_row)) / bsize_row + 1
  endif
  write(*, *) 'Size of block', bsize_row

  B_IC%n_belem_row = max_block_row
  allocate(B_IC%row_ptr(max_block_row+1), BCCS%col_ptr(max_block_col+1), temp_col(max_block_row), ind_to_bid(matA%row_ptr(n+1)-1))
  allocate(pivot(bsize_row))
  
  BCCS%col_ptr = 0
  B_IC%row_ptr(1) = 1
  do k = 1, n, bsize_row !Count number of blocks
     
     bind_row = (k - mod(k-1, bsize_row)) / bsize_row + 1
     temp_col = 0
     do j = k, k+bsize_row-1 !Count number of blocks in bind_row-th block-row-index
        if(j > n) exit
        do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
           if(matA%col_ind(i) < j) cycle
           bind_col = (matA%col_ind(i) - mod(matA%col_ind(i)-1, bsize_row)) / bsize_row + 1
           temp_col(bind_col) = 1
        enddo
     enddo

     B_IC%row_ptr(bind_row+1) = B_IC%row_ptr(bind_row)
     count = B_IC%row_ptr(bind_row)
     flag = .false.
     do i = 1, max_block_row !Calculate row_ptr and check id of bind_row-th blocks
        if(temp_col(i) == 1) then
           if(flag) then
              BCCS%col_ptr(i+1) = BCCS%col_ptr(i+1) + 1
              B_IC%row_ptr(bind_row+1) = B_IC%row_ptr(bind_row+1) + 1
              temp_col(i) = count
              count = count + 1
           else
              flag = .true.
              temp_col(i) = 0
           endif
        endif
     enddo
     
     do j = k, k+bsize_row-1 !Coordinate matA%val and block id
        if(j > n) exit
        do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
           if(matA%col_ind(i) < j) cycle
           bind_col = (matA%col_ind(i) - mod(matA%col_ind(i)-1, bsize_row)) / bsize_row + 1
           ind_to_bid(i) = temp_col(bind_col)
        enddo
     enddo
  enddo 

  BCCS%col_ptr(1) = 1
  do i = 2, max_block_col+1 !Make column poiter
     BCCS%col_ptr(i) = BCCS%col_ptr(i) + BCCS%col_ptr(i-1)
  enddo
  
  allocate(B_IC%val(bsize_row, bsize_col, B_IC%row_ptr(max_block_row+1)-1), B_IC%dgn(bsize_row, bsize_col, max_block_row), &
           B_IC%inv_dgn(bsize_row, bsize_col, max_block_row))
  allocate(B_IC%col_ind(B_IC%row_ptr(max_block_row+1)-1), B_IC%col_bind(B_IC%row_ptr(max_block_row+1)-1))
  allocate(BCCS%row_bind(B_IC%row_ptr(max_block_row+1)-1), BCCS%rev_ind(B_IC%row_ptr(max_block_row+1)-1))

  BCCS%row_bind = 0
  
  B_IC%val = 0.0d0
  B_IC%dgn = 0.0d0
  B_IC%col_ind = 0
  do j = 1, n !Put val and col to B_IC
     bind_row = (j - mod(j-1, bsize_row)) / bsize_row + 1
     inb_ind_row = mod(j-1, bsize_row) + 1
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        if(matA%col_ind(i) < j) cycle
        inb_ind_col = mod(matA%col_ind(i)-1, bsize_col) + 1
        if(ind_to_bid(i) == 0) then
           B_IC%dgn(inb_ind_row, inb_ind_col, bind_row) = matA%val(i)
        else
           B_IC%val(inb_ind_row, inb_ind_col, ind_to_bid(i)) = matA%val(i)
           if(B_IC%col_ind(ind_to_bid(i)) == 0) then
              B_IC%col_ind(ind_to_bid(i)) = matA%col_ind(i) - inb_ind_col + 1
              B_IC%col_bind(ind_to_bid(i)) = (B_IC%col_ind(ind_to_bid(i))-1) / bsize_col + 1
              do h = BCCS%col_ptr(B_IC%col_bind(ind_to_bid(i))), BCCS%col_ptr(B_IC%col_bind(ind_to_bid(i))+1)-1
                 if(BCCS%row_bind(h) == 0) then
                    BCCS%row_bind(h) = bind_row
                    BCCS%rev_ind(h) = ind_to_bid(i)
                    exit
                 endif
              enddo
           endif
        endif
     enddo
  enddo

  do i = bsize_row-B_IC%padding+1, bsize_row  !Fill last row_block without any datas.
     B_IC%dgn(i, i, max_block_row) = 1.0d0
  enddo

  B_IC%n_belem_row_glb = B_IC%n_belem_row
  abmc%num_color = aflags%color
  
  select case(aflags%color_method)
  case(1)
     call AMC_iwst(B_IC%row_ptr, B_IC%col_bind, abmc, status_ord)
  case(2)
     call MC_greedy(B_IC%row_ptr, B_IC%col_bind, abmc, status_ord)
  case(3)
     call expand_up_to_low(B_IC%row_ptr, B_IC%col_bind, exp_row_ptr, exp_col_bind)
     call MC_metis_study(exp_row_ptr, exp_col_bind, abmc, aflags%grping, status_ord)
     deallocate(exp_row_ptr, exp_col_bind)
  case(4)
     call expand_up_to_low(B_IC%row_ptr, B_IC%col_bind, exp_row_ptr, exp_col_bind)
     call MC_metis_study(exp_row_ptr, exp_col_bind, abmc, aflags%grping, status_ord)
     call reverse_order(abmc)
     deallocate(exp_row_ptr, exp_col_bind)
  case(5)
     call expand_up_to_low(B_IC%row_ptr, B_IC%col_bind, exp_row_ptr, exp_col_bind)
     call CM_AMC_iwst(exp_row_ptr, exp_col_bind, abmc, status_ord)
     deallocate(exp_row_ptr, exp_col_bind)
  case(6)
     call expand_up_to_low(B_IC%row_ptr, B_IC%col_bind, exp_row_ptr, exp_col_bind)
     call CM_AMC_iwst(exp_row_ptr, exp_col_bind, abmc, status_ord)
     call reverse_order(abmc)
     deallocate(exp_row_ptr, exp_col_bind)
  case(7)
     call expand_up_to_low(B_IC%row_ptr, B_IC%col_bind, exp_row_ptr, exp_col_bind)
     call CM_MC_Greedy(exp_row_ptr, exp_col_bind, abmc, status_ord)
     deallocate(exp_row_ptr, exp_col_bind)
  case(8)
     call expand_up_to_low(B_IC%row_ptr, B_IC%col_bind, exp_row_ptr, exp_col_bind)
     call CM_MC_Greedy(exp_row_ptr, exp_col_bind, abmc, status_ord)
     call reverse_order(abmc)
     deallocate(exp_row_ptr, exp_col_bind)
  end select

  if(status_ord /= 0) return
  
  !Norimalization(unitting) each row####################
  select case (aflags%normalize)
  case(0)
     write(*, *) 'Un-normalization'
     do k = 1, max_block_row !Copy upper data to lower in a diagonal matrix and shiftin diagonal. This part is not corresponded to the symmetric matrices.
        do j = 1, B_IC%bsize_row        
           do i = j + 1, B_IC%bsize_row
              B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
           enddo
        enddo
     enddo
  case(1)
     allocate(B_IC%inv_sqrt(n+B_IC%padding))
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           B_IC%inv_sqrt((k-1)*B_IC%bsize_row+j) = 1.0d0 / sqrt(abs(B_IC%dgn(j, j, k)))
           do i = j+1, B_IC%bsize_col
              B_IC%dgn(j, i, k) = B_IC%dgn(j, i, k) * B_IC%inv_sqrt((k-1)*B_IC%bsize_row+j) / sqrt(abs(B_IC%dgn(i, i, k)))
              B_IC%dgn(i, j, k) = B_IC%dgn(j, i, k)
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_col
                 B_IC%val(i, h, j) = B_IC%val(i, h, j) * B_IC%inv_sqrt((k-1)*B_IC%bsize_row+i) / sqrt(abs(B_IC%dgn(h, h, B_IC%col_bind(j))))
              enddo
           enddo
        enddo
     enddo
  end select

  !End Normalization##################

   !Diagonal shifting ######################
  select case (aflags%shift_method)
  case(0)
     write(*, *) 'No shifting'
  case(1)
     write(*, *) 'Adding shift with', aflags%shift_r
     do k = 1, max_block_row 
        do j = 1, B_IC%bsize_row
           B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + aflags%shift_r
        enddo
     enddo
  case(2)
     write(*, *) 'Multiply shift with', aflags%shift_r
     do k = 1, max_block_row 
        do j = 1, B_IC%bsize_row
           B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) * aflags%shift_r
        enddo
     enddo
  case(3)
     write(*, *) 'Adding shift which is sum of off-diagonals'
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(i /= j) B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + B_IC%dgn(j, i, k)
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_row
                 B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) + B_IC%val(i, h, j)
                 B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) + B_IC%val(i, h, j)
              enddo
           enddo
        enddo
     enddo
  case(4)
     temp_i = 0
     do j = 1, B_IC%n_belem_row
        do i = 1, B_IC%bsize_row
           if(real(B_IC%dgn(i, i, j)) >= 0.0d0) then
              temp_i = temp_i + 1
           else
              temp_i = temp_i - 1
           endif
        enddo
     enddo
     write(*, *) 'Adding shift which is sum of absolute-off-diagonals', temp_i
     do k = 1, B_IC%n_belem_row
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(i /= j) then
                 if(temp_i >= 0) then
                    B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) + abs(B_IC%dgn(j, i, k))
                 else
                    B_IC%dgn(j, j, k) = B_IC%dgn(j, j, k) - abs(B_IC%dgn(j, i, k))
                 endif
              endif
           enddo
        enddo
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_row
              do h = 1, B_IC%bsize_row
                 if(temp_i >= 0) then
                    B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) + abs(B_IC%val(i, h, j))
                    B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) + abs(B_IC%val(i, h, j))
                 else
                    B_IC%dgn(i, i, k) = B_IC%dgn(i, i, k) - abs(B_IC%val(i, h, j))
                    B_IC%dgn(h, h, B_IC%col_bind(j)) = B_IC%dgn(h, h, B_IC%col_bind(j)) - abs(B_IC%val(i, h, j))
                 endif
              enddo
           enddo
        enddo
     enddo
  case(5)
     temp_i = 0
     do j = 1, B_IC%n_belem_row
        do i = 1, B_IC%bsize_row
           if(real(B_IC%dgn(i, i, j)) >= 0.0d0) then
              temp_i = temp_i + 1
           else
              temp_i = temp_i - 1
           endif
        enddo
     enddo
     write(*, *) 'Adding shift which is sum of off-diagonal blocks', temp_i
     allocate(sum_off(B_IC%n_belem_row))
     sum_off = 0.0d0
     do k = 1, B_IC%n_belem_row
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           do i = 1, B_IC%bsize_col
              do h = 1, B_IC%bsize_row
                 if(temp_i >= 0) then
                    sum_off(k) = sum_off(k) + abs(B_IC%val(h, i, j))
                    sum_off(B_IC%col_bind(j)) = sum_off(B_IC%col_bind(j)) + abs(B_IC%val(h, i, j))
                 else
                    sum_off(k) = sum_off(k) - abs(B_IC%val(h, i, j))
                    sum_off(B_IC%col_bind(j)) = sum_off(B_IC%col_bind(j)) - abs(B_IC%val(h, i, j))
                 endif
              enddo
           enddo
        enddo
     enddo
     min_r = sum_off(1)
     max_r = sum_off(1)
     sum_r = 0.0d0
     do i = 2, B_IC%n_belem_row
        if(sum_off(i) < min_r) min_r = sum_off(i)
        if(sum_off(i) > max_r) max_r = sum_off(i)
        sum_r = sum_r + sum_off(i) 
     enddo
     write(*, *) 'Check shift param', min_r/ B_IC%bsize_row, max_r/ B_IC%bsize_row, sum_r/B_IC%n_belem_row/ B_IC%bsize_row
     do j = 1, B_IC%n_belem_row
        do i = 1, B_IC%bsize_row
           if(temp_i >= 0.0d0) then
              B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) + sum_off(j) / B_IC%bsize_row * aflags%shift_r
           else
              B_IC%dgn(i, i, j) = B_IC%dgn(i, i, j) - sum_off(j) / B_IC%bsize_row * aflags%shift_r
           endif
        enddo
     enddo
  end select

  !End Diagonal shifting ##################

  allocate(bmatA_back%val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1), bmatA_back%row_ptr(B_IC%n_belem_row+1) &
         , bmatA_back%col_bind(B_IC%row_ptr(B_IC%n_belem_row+1)-1), bmatA_back%col_ind(B_IC%row_ptr(B_IC%n_belem_row+1)-1) &
         , row_count(B_IC%n_belem_row))
  bmatA_back%val = B_IC%val
  bmatA_back%col_bind = B_IC%col_bind
  bmatA_back%col_ind = B_IC%col_ind
  bmatA_back%row_ptr = B_IC%row_ptr
  allocate(logic_row(B_IC%n_belem_row),  reod(B_IC%row_ptr(B_IC%n_belem_row+1)-1))
  logic_row = .false.
  row_count = 0
  do icolor = 1, abmc%num_color
     do kk = 1, abmc%color(icolor)%num_elems
        k = abmc%color(icolor)%elem(kk)
        logic_row(k) = .true.
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           if(logic_row(B_IC%col_bind(j))) then !Move calculated blocks to lower part with transporting.
              reod(j) = B_IC%col_bind(j)
              row_count(B_IC%col_bind(j)) = row_count(B_IC%col_bind(j)) + 1
           else
              reod(j) = k
              row_count(k) = row_count(k) + 1
           endif
        enddo

     enddo
  enddo

  B_IC%row_ptr(1) = 1
  do i = 2, B_IC%n_belem_row+1
     B_IC%row_ptr(i) = B_IC%row_ptr(i-1) + row_count(i-1)
  enddo
  if(B_IC%row_ptr(B_IC%n_belem_row+1) /= bmatA_back%row_ptr(B_IC%n_belem_row+1)) then
     write(*, *) 'Check, reordering or swapping', B_IC%row_ptr(B_IC%n_belem_row+1), bmatA_back%row_ptr(B_IC%n_belem_row+1)
     stop
  endif

  row_count = B_IC%row_ptr(1:B_IC%n_belem_row)
  do j = 1, B_IC%n_belem_row
     do i = bmatA_back%row_ptr(j), bmatA_back%row_ptr(j+1)-1
        if(reod(i) == j) then
           B_IC%val(:, :, row_count(j)) = bmatA_back%val(:, :, i)
           B_IC%col_bind(row_count(j)) = bmatA_back%col_bind(i)
           B_IC%col_ind(row_count(j)) = bmatA_back%col_ind(i)
           row_count(j) = row_count(j) + 1
        else
           B_IC%val(:, :, row_count(reod(i))) = transpose(bmatA_back%val(:, :, i))
           B_IC%col_bind(row_count(reod(i))) = j
           B_IC%col_ind(row_count(reod(i))) = (j - 1) * B_IC%bsize_row + 1
           row_count(reod(i)) = row_count(reod(i)) + 1
        endif
     enddo
  enddo
  deallocate(bmatA_back%val, bmatA_back%col_bind, bmatA_back%col_ind, bmatA_back%row_ptr)
  deallocate(reod, row_count)

  !Remake BCCS
  allocate(count_col(B_IC%n_belem_row))
  count_col = 0
  do k = 1, B_IC%n_belem_row
     do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
        count_col(B_IC%col_bind(j)) = count_col(B_IC%col_bind(j)) + 1
     enddo
  enddo
  BCCS%col_ptr(1) = 1
  do i = 2, B_IC%n_belem_row+1
     BCCS%col_ptr(i) = BCCS%col_ptr(i-1) + count_col(i-1)
  enddo
  count_col = 0
  do k = 1, B_IC%n_belem_row
     do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
        i = B_IC%col_bind(j)
        BCCS%row_bind(BCCS%col_ptr(i)+ count_col(i)) = k
        BCCS%rev_ind(BCCS%col_ptr(i)+ count_col(i)) = j
        count_col(i) = count_col(i) + 1
     enddo
  enddo

  n_belem_max = 0
  do i = 1, B_IC%n_belem_row
     if(n_belem_max < B_IC%row_ptr(i+1)-B_IC%row_ptr(i)) n_belem_max = B_IC%row_ptr(i+1)-B_IC%row_ptr(i)
  enddo  
  allocate(temp_mat(bsize_row, bsize_col, n_belem_max))
  
  logic_row = .false.
  do icolor = 1, abmc%num_color

     do kk = 1, abmc%color(icolor)%num_elems
        k = abmc%color(icolor)%elem(kk)
        logic_row(k) = .true.

        do j = 1, B_IC%bsize_row
           if(abs(B_IC%dgn(j, j, k)) <= 1.0d0-7) then
              write(*, *) 'Diagonal is too small. Check Inputs or Shift.', B_IC%dgn(j, j, k)
              stop
           endif
        enddo
        B_IC%inv_dgn(:, :, k) = B_IC%dgn(:, :, k)
        call GETRF(B_IC%inv_dgn(:, :, k), pivot)
        call GETRI(B_IC%inv_dgn(:, :, k), pivot)
        !Checking process of invarse#########################
        flag = .true.
        temp_mat(:, :, 1) = 0.0d0 
        temp_mat(:, :, 1) = matmul(B_IC%dgn(:, :, k), B_IC%inv_dgn(:, :, k))
        do j = 1, B_IC%bsize_row
           do i = 1, B_IC%bsize_row
              if(((abs(1.0d0-abs(temp_mat(i, j, 1))) >= 1.0d-7 .and. i == j) .or. (abs(temp_mat(i, j, 1)) >= 1.0d-7 .and. i /= j)) .and. flag)then
                 write(*, *) 'Check inv of dgn', temp_mat(i, j, 1), i, j, k
                 do h = 1, B_IC%bsize_row
                    write(*, '(4e12.4)') B_IC%dgn(h, :, k)
                 enddo
                 do h = 1, B_IC%bsize_row
                    write(*, '(4e12.4)') B_IC%inv_dgn(h, :, k)
                 enddo
                 do h = 1, B_IC%bsize_row
                    write(*, '(4e12.4, i2)') temp_mat(h, :, 1), pivot(h)
                 enddo
                 flag = .false.
              endif
           enddo
        enddo
        !End checking process of invarse#########################

        temp_col = 0
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           temp_col(B_IC%col_bind(j)) = j
        enddo

        count = 1
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           temp_mat(:, :, count) = B_IC%val(:, :, j)
           B_IC%val(:, :, j) = matmul(B_IC%inv_dgn(:, :, k), B_IC%val(:, :, j))
           count = count + 1
        enddo

        count = 1
        do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
           
           do i = BCCS%col_ptr(B_IC%col_bind(j)), BCCS%col_ptr(B_IC%col_bind(j)+1)-1
              if(temp_col(BCCS%row_bind(i)) /= 0 .and. (.not. logic_row(BCCS%row_bind(i)))) then
                 B_IC%val(:, :, BCCS%rev_ind(i)) = B_IC%val(:, :, BCCS%rev_ind(i)) &
                    - matmul(transpose(B_IC%val(:, :, temp_col(BCCS%row_bind(i)))), temp_mat(:, :, count))
              endif
           enddo
           B_IC%val(:, :, BCCS%rev_ind(i)) = B_IC%val(:, :, BCCS%rev_ind(i)) &
              - matmul(transpose(B_IC%val(:, :, temp_col(BCCS%row_bind(i)))), temp_mat(:, :, count))
        enddo
     enddo
  enddo
     
  count = 0
  do k = 1, B_IC%row_ptr(B_IC%n_belem_row+1)-1
     do j = 1, B_IC%bsize_row
        do i = 1, B_IC%bsize_col
           if(B_IC%val(j, i, k) /= 0) count = count + 1
        enddo
     enddo
  enddo
  count = count * 2
  do k = 1, B_IC%n_belem_row
     do j = 1, B_IC%bsize_row
        do i = 1, B_IC%bsize_col
           if(B_IC%dgn(j, i, k) /= 0) count = count + 1
        enddo
     enddo
  enddo
  count = count - B_IC%padding
  write(*, *) 'Number of Nonzer element of B_IC is', count, ', block is ', (B_IC%row_ptr(B_IC%n_belem_row+1)-1)*2+B_IC%n_belem_row, &
     ', filling rate is ', dble(count)/dble(((B_IC%row_ptr(B_IC%n_belem_row+1)-1)*2+B_IC%n_belem_row)*(B_IC%bsize_row**2)), '.'
  write(*, *) 'Number of Nonzer element of matA is', matA%row_ptr(n+1)-1, ', filling rate is ' &
     , dble(matA%row_ptr(n+1)-1) / dble(((B_IC%row_ptr(B_IC%n_belem_row+1)-1)*2+B_IC%n_belem_row)*(B_IC%bsize_row**2)), '.'

end subroutine M_Para_Block_I_Cholesky

subroutine M_BICCG(CRS_A, B_IC, B, x, n, abmc, itrmax, a_err, aflags)
  use coefficient
  use execarguments
  use mod_parallel
  implicit none
  include 'mpif.h'
  integer, intent(in) :: n
  integer, intent(in) :: itrmax
  real(8), intent(in) :: a_err
  double precision, intent(in) :: B(n)
  double precision, intent(inout) :: x(n)
  type(CRS_mat), intent(in) :: CRS_A
  type(BCRS_mat), intent(in) :: B_IC
  type(ord_para), intent(in) :: abmc
  type(argflags), intent(in) :: aflags
  type(temporary) :: temp_sol
  integer :: itr, i, j, h, k, row_ind, fo = 10, count
  integer idx, icolor, proc, max
  double precision alpha, beta, gamma, gamma_local, rou, rou_local, nyu, nyu_local, err, err_local, sum_temp
  real(8) f_err
  double precision, allocatable :: r(:), p(:), q(:), temp(:)
  character(100) :: fname
  integer :: MCW=MPI_COMM_WORLD, me_proc, num_proc, ierr
  integer, allocatable :: req(:), st_is(:, :)
  integer :: st(MPI_STATUS_SIZE) 
  
  interface
     subroutine M_solveP_Block(B_IC, z, r, n, abmc, temp_a)
       use coefficient
       use mod_parallel
       implicit none
       integer, intent(in) :: n
       double precision, intent(in) :: r(:)
       double precision, intent(out) :: z(:)
       type(BCRS_mat), intent(in) :: B_IC
       type(ord_para), intent(in) :: abmc
       type(temporary), intent(inout) :: temp_a
     end subroutine M_solveP_Block

     subroutine check_sol(matA, r, z, n)
       use coefficient
       implicit none
       double precision, intent(in) :: r(:)
       double precision, intent(out) :: z(:)
       type(CRS_mat), intent(in) :: matA
       integer, intent(in) :: n
     end subroutine check_sol
       
  end interface

  call MPI_Comm_size(MCW ,num_proc ,ierr)
  call MPI_Comm_rank(MCW ,me_proc ,ierr)
  me_proc = me_proc + 1

  allocate(req(num_proc), st_is(MPI_STATUS_SIZE, num_proc))
  
  allocate(r(n+B_IC%padding), p(n+B_IC%padding), q(n+B_IC%padding), temp(n+B_IC%padding))
  allocate(temp_sol%sub(n+B_IC%padding), temp_sol%zd(n+B_IC%padding)) !, temp_sol%sub_r(n+B_IC%padding))

  allocate(temp_sol%ind_fwd(num_proc, abmc%num_color), temp_sol%ind_bck(num_proc, abmc%num_color), temp_sol%ind_matvec(num_proc))
  temp_sol%ind_fwd(1, 1:abmc%num_color) = 1
  temp_sol%ind_bck(1, 1:abmc%num_color) = 1  
  max = 0
  do icolor = 1, abmc%num_color
     do proc = 2, num_proc
        temp_sol%ind_fwd(proc, icolor) = temp_sol%ind_fwd(proc-1, icolor) + abmc%color(icolor)%buf_send_tindex(proc-1)%num*B_IC%bsize_row
        temp_sol%ind_bck(proc, icolor) = temp_sol%ind_bck(proc-1, icolor) + abmc%color(icolor)%buf_recv_tindex(proc-1)%num*B_IC%bsize_row
     enddo
     if(max < temp_sol%ind_fwd(num_proc, icolor) + abmc%color(icolor)%buf_send_tindex(num_proc)%num*B_IC%bsize_row - 1) &
        max = temp_sol%ind_fwd(num_proc, icolor) + abmc%color(icolor)%buf_send_tindex(num_proc)%num*B_IC%bsize_row - 1
     if(max < temp_sol%ind_bck(num_proc, icolor) + abmc%color(icolor)%buf_recv_tindex(num_proc)%num*B_IC%bsize_row - 1) &
        max = temp_sol%ind_bck(num_proc, icolor) + abmc%color(icolor)%buf_recv_tindex(num_proc)%num*B_IC%bsize_row - 1
  enddo
  temp_sol%ind_matvec(1) = 1  
  do proc = 2, num_proc
     temp_sol%ind_matvec(proc) = temp_sol%ind_matvec(proc-1) + abmc%buf_recv_tindex(proc-1)%num
  enddo

  if(max < temp_sol%ind_matvec(num_proc) + abmc%buf_recv_tindex(num_proc)%num - 1) &
       max = temp_sol%ind_matvec(num_proc) + abmc%buf_recv_tindex(num_proc)%num - 1
  allocate(temp_sol%sub_r(max))
  
  count = 1
  do proc = 1, num_proc
     if(proc /= me_proc .and. abmc%send_count(proc) /= 0) then
        call MPI_Isend(x, abmc%send_count(proc), abmc%send_whole_type(proc), proc-1, 0, MCW, req(count), ierr)
        count = count + 1
     endif
  enddo
  do proc = 1, num_proc
     if(proc /= me_proc .and. abmc%recv_count(proc) /= 0) then
        call MPI_recv(temp_sol%sub_r(temp_sol%ind_matvec(proc)), abmc%buf_recv_tindex(proc)%num, MPI_DOUBLE_PRECISION, proc-1, 0, MCW, st, ierr)
     endif
  enddo
  call MPI_Waitall(count-1, req, st_is, ierr)
  do proc = 1, num_proc
     if(proc /= me_proc .and. abmc%recv_count(proc) /= 0) then
        do h = 1, abmc%buf_recv_tindex(proc)%num
           idx = abmc%buf_recv_tindex(proc)%disp(h)+1
           x(idx) = temp_sol%sub_r(temp_sol%ind_matvec(proc)+h-1)
        enddo
     endif
  enddo

  
  call calc_residual(CRS_A, x, B, r(1:n), n, abmc)
  err_local = dot_product(r(abmc%row_start:abmc%row_end), r(abmc%row_start:abmc%row_end))
  call MPI_Allreduce(err_local, err, 1, MPI_Double_precision, MPI_SUM, MCW, ierr)
  f_err = sqrt(err)
  r(n+1:n+B_IC%padding) = 0.0d0

  temp(1:n+B_IC%padding) = 0.0d0
  p(1:n+B_IC%padding) = 0.0d0
  q(1:n+B_IC%padding) = 0.0d0
  temp_sol%sub(1:n+B_IC%padding) = 0.0d0
  temp_sol%sub_r(1:max) = 0.0d0
  temp_sol%zd(1:n+B_IC%padding) = 0.0d0
  
  call M_solveP_Block(B_IC, p, r, n, abmc, temp_sol)

  rou = 0.0d0
  rou_local = dot_product(r(abmc%row_start:abmc%row_end), p(abmc%row_start:abmc%row_end))
  call MPI_Allreduce(rou_local, rou, 1, MPI_Double_precision, MPI_SUM, MCW, ierr)
  
  do itr = 1, itrmax
     
     count = 1
     do proc = 1, num_proc
        if(proc /= me_proc .and. abmc%send_count(proc) /= 0) then
           call MPI_Isend(p, abmc%send_count(proc), abmc%send_whole_type(proc), proc-1, 0, MCW, req(count), ierr)
           count = count + 1
        endif
     enddo
     do proc = 1, num_proc
        if(proc /= me_proc .and. abmc%recv_count(proc) /= 0) then
           call MPI_recv(temp_sol%sub_r(temp_sol%ind_matvec(proc)), abmc%buf_recv_tindex(proc)%num, MPI_DOUBLE_PRECISION, proc-1, &
                         0, MCW, st, ierr)
        endif
     enddo
     call MPI_Waitall(count-1, req, st_is, ierr)
     do proc = 1, num_proc
        if(proc /= me_proc .and. abmc%recv_count(proc) /= 0) then
           do h = 1, abmc%buf_recv_tindex(proc)%num
              idx = abmc%buf_recv_tindex(proc)%disp(h)+1
              p(idx) = temp_sol%sub_r(temp_sol%ind_matvec(proc)+h-1)
           enddo
        endif
     enddo
      
      do j = 1, CRS_A%n_row 
        q(j+abmc%row_start-1) = 0.0d0
        do i = CRS_A%row_ptr(j), CRS_A%row_ptr(j+1)-1
           q(j+abmc%row_start-1) = q(j+abmc%row_start-1) + CRS_A%val(i) * p(CRS_A%col_ind(i))
        enddo
     enddo

     gamma = 0.0d0
     gamma_local = dot_product(p(abmc%row_start:abmc%row_end), q(abmc%row_start:abmc%row_end))
     call MPI_Allreduce(gamma_local, gamma, 1, MPI_Double_precision, MPI_SUM, MCW, ierr)

     alpha = rou / gamma
     
     x(abmc%row_start:abmc%row_end) = x(abmc%row_start:abmc%row_end) + alpha * p(abmc%row_start:abmc%row_end)
     r(abmc%row_start:abmc%row_end) = r(abmc%row_start:abmc%row_end) - alpha * q(abmc%row_start:abmc%row_end) 

     err = 0.0d0
     err_local = dot_product(r(abmc%row_start:abmc%row_end), r(abmc%row_start:abmc%row_end))
     call MPI_Allreduce(err_local, err, 1, MPI_Double_precision, MPI_SUM, MCW, ierr)
     if(me_proc == 1) write(*, *) 'itr = ', itr, 'err = ', sqrt(err)/f_err

     if ( sqrt(err)/f_err <= a_err) then
        if(me_proc == 1) write(*, *) 'convarged', itr
        exit
     endif

     call M_solveP_Block(B_IC, q, r, n, abmc, temp_sol)

     nyu = 0.0d0
     nyu_local = dot_product(q(abmc%row_start:abmc%row_end), r(abmc%row_start:abmc%row_end))
     call MPI_Allreduce(nyu_local, nyu, 1, MPI_Double_precision, MPI_SUM, MCW, ierr)
     beta = nyu / rou
     p(abmc%row_start:abmc%row_end) = q(abmc%row_start:abmc%row_end) + beta * p(abmc%row_start:abmc%row_end)
     rou = nyu
  enddo

  count = 1
  do proc = 1, num_proc
     if(proc /= me_proc .and. abmc%send_count(proc) /= 0) then
        call MPI_Isend(x, abmc%send_count(proc), abmc%send_whole_type(proc), proc-1, 0, MCW, req(count), ierr)
        count = count + 1
     endif
  enddo
  do proc = 1, num_proc
     if(proc /= me_proc .and. abmc%recv_count(proc) /= 0) then
        call MPI_recv(temp_sol%sub_r(temp_sol%ind_matvec(proc)), abmc%buf_recv_tindex(proc)%num, MPI_DOUBLE_PRECISION, proc-1, 0, MCW, st, ierr)
     endif
  enddo
  call MPI_Waitall(count-1, req, st_is, ierr)
  do proc = 1, num_proc
     if(proc /= me_proc .and. abmc%recv_count(proc) /= 0) then
        do h = 1, abmc%buf_recv_tindex(proc)%num
           idx = abmc%buf_recv_tindex(proc)%disp(h)+1
           x(idx) = temp_sol%sub_r(temp_sol%ind_matvec(proc)+h-1)
        enddo
     endif
  enddo

  call calc_residual(CRS_A, x, B, r(1:n), n, abmc)
  err = 0.0d0
  err_local = dot_product(r(abmc%row_start:abmc%row_end), r(abmc%row_start:abmc%row_end))
  call MPI_Allreduce(err_local, err, 1, MPI_Double_precision, MPI_SUM, MCW, ierr)
  if(me_proc == 1) write(*, *) 'Check final', sqrt(err) / f_err
  
end subroutine M_BICCG

subroutine Jacobi(A, z, r, n)
  use coefficient
  implicit none
  integer, intent(in) :: n
  double precision, intent(in) :: r(n)
  double precision, intent(out) :: z(n)
  type(CRS_mat), intent(in) :: A
  integer i, j, k
  integer maxitr, itr
  double precision err0, diag, weight, err, ferr
  double precision, allocatable :: temp(:)
  
  err0 = 1.0d-3
  maxitr = 10
  weight = 1.0d0
  allocate(temp(n))

  ! ferr = sqrt(dot_product(r, r))

  z = 0.0d0
  do itr = 1, maxitr
  
     do j = 1, n
        temp(j) = r(j)
        do i = A%row_ptr(j)+1, A%row_ptr(j+1)-1
           temp(j) = temp(j) - A%val(i) * z(A%col_ind(i))
        enddo
     enddo
     do j = 1, n
        temp(j) = temp(j) / A%val(A%row_ptr(j))
     enddo
     z = (1.0d0 - weight) * z + weight * temp
     
  enddo
     
end subroutine Jacobi

subroutine calc_residual(A, z, B, r, n, abmc)
  use coefficient
  use mod_parallel
  implicit none
  include 'mpif.h'
  integer, intent(in) :: n
  double precision, intent(in) :: z(n), B(n)
  double precision, intent(out) :: r(n)
  type(CRS_mat), intent(in) :: A
  type(ord_para), intent(in) :: abmc
  integer i, j, k

  r = B

  do j = 1, A%n_row
     do i = A%row_ptr(j), A%row_ptr(j+1)-1
        r(j+abmc%row_start-1) = r(j+abmc%row_start-1) - A%val(i) * z(A%col_ind(i))
     enddo
  enddo
        
end subroutine Calc_Residual

function mat_vec_mult(mat, vec, size)
  integer, intent(in) :: size
  double precision, intent(in) :: mat(:, :), vec(:)
  double precision :: mat_vec_mult(size)
  integer i, j

  mat_vec_mult = 0.0d0
  do j = 1, size
     do i = 1, size
        mat_vec_mult(i) = mat_vec_mult(i) + mat(i, j) * vec(j)
        ! write(*, *) 'matvec', mat_vec_mult(i), mat(i, j), vec(j), mat(i, j) * vec(j)
     enddo
  enddo

end function mat_vec_mult

subroutine check_ic(B_IC, BCCS, matA, bmatA_back)
  use coefficient
  implicit none
  type(CRS_mat), intent(in) :: matA
  type(BCRS_mat), intent(in) :: B_IC, bmatA_back
  type(CCS_mat_ind), intent(in) :: BCCS
  type(CRS_mat) :: matA_check
  type(BCRS_mat) :: bmatA_check
  integer i, j, k, ind_row, ind_col

  allocate(bmatA_check%val(B_IC%bsize_row, B_IC%bsize_col, B_IC%row_ptr(B_IC%n_belem_row+1)-1))
  
  do k = 1, B_IC%n_belem_row
     do j = B_IC%row_ptr(k), B_IC%row_ptr(k+1)-1
        bmatA_check%val(:, :, j) = 0.0d0
        do i = BCCS%col_ptr(j), BCCS%col_ptr(j+1)-1
           ind_row = 1
           ind_col = 1
           bmatA_check%val(:, :, j) = bmatA_check%val(:, :, j) + matmul(transpose(B_IC%val(:, :, ind_row)), B_IC%val(:, :, ind_col))
        enddo
     enddo
  enddo
  
end subroutine check_ic

subroutine check_dense(B_IC, matA, n)
  use coefficient
  implicit none
  integer, intent(in) :: n
  type(BCRS_mat), intent(in) :: B_IC
  type(CRS_mat), intent(in) :: matA
  double precision, allocatable :: iA_org(:, :), iA_BIC(:, :), iL(:, :), iU(:, :), iD(:, :), A_res(:, :), iD_inv(:, :)
  integer, allocatable :: pivot(:)
  integer i, j, k, l, ind_row, ind_col

  allocate(iA_org(n, n), iA_BIC(n, n), pivot(n), iL(n, n), iU(n, n), iD(n, n), A_res(n, n), iD_inv(n, n))

  iA_org = 0.0d0
  iA_BIC = 0.0d0
  
  do j = 1, n
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        iA_org(j, matA%col_ind(i)) = matA%val(i)
     enddo
  enddo
  do j = 1, n
     do i = j+1, n
        iA_org(i, j) = iA_org(j, i)
     enddo
  enddo
  
  write(*, *) 'Original'
  do i = 1, n
     write(*, *) iA_org(i, :)
  enddo

  iL = 0.0d0
  iU = 0.0d0
  iD = 0.0d0
  do i = 1, n
     iL(i, i) = 1.0d0
     iU(i, i) = 1.0d0
     iD(i, i) = 1.0d0
  enddo

  do l = 1, B_IC%n_belem_row
     ind_row = B_IC%bsize_row * (l-1)
     do j = 1, B_IC%bsize_col
        if(ind_row+j > n) exit
        do i = 1, B_IC%bsize_row
           if(ind_row+i > n) exit
           iA_BIC(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD_inv(ind_row+i, ind_row+j) = B_IC%inv_dgn(i, j, l)
        enddo
     enddo
     do k = B_IC%row_ptr(l), B_IC%row_ptr(l+1)-1
        do j = 1, B_IC%bsize_col
           if(B_IC%col_ind(k)+j-1 > n) exit
           do i = 1, B_IC%bsize_row
              if(ind_row+i > n) exit
              ! write(*, *) 'In Loop', i, j, k, ind_row+i, B_IC%col_ind(k)+j-1
              iA_BIC(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iU(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iL(B_IC%col_ind(k)+j-1, ind_row+i) = B_IC%val(i, j, k)
           enddo
        enddo
     enddo
  enddo

  ! call LA_GETRF(iA_org, pivot)
  ! call LA_GETRI(iA_org, pivot)

  ! write(*, *) B_IC%inv_dgn
  ! write(*, *) B_IC%val
  
  ! write(*, *) 'Inverse of lapack'
  ! do i = 1, n
  !    write(*, *) iA_org(i, :)
  ! enddo

  write(*, *) 'Inverse of B_IC'
  do i = 1, n
     write(*, *) iA_BIC(i, :)
  enddo

  write(*, *) 'Pivot', pivot

  write(*, *) 'Inverse of iL'
  do i = 1, n
     write(*, *) iL(i, :)
  enddo
  write(*, *) 'Inverse of iD'
  do i = 1, n
     write(*, *) iD(i, :)
  enddo
  write(*, *) 'Inverse of iU'
  do i = 1, n
     write(*, *) iU(i, :)
  enddo
  write(*, *) 'Inverse of iD_inv'
  do i = 1, n
     write(*, *) iD_inv(i, :)
  enddo

  A_res = 0.0d0
  A_res = matmul(iL, iD)
  A_res = matmul(A_res, iU)
  
  write(*, *) 'matmul LDU'
  do i = 1, n
     write(*, *) A_res(i, :)
  enddo
  write(*, *) 'inv_D'
  do i = 1, B_IC%n_belem_row
     write(*, *) B_IC%inv_dgn(:, :, i)
  enddo
  
  ! do i = 1, n
  !    do j = i, n
  !       if(abs(iA_org(i, j) - iA_BIC(i, j)) >= 1.0d0-7) write(*, *) 'Check_diff', i, j, iA_org(i, j), iA_BIC(i, j), pivot(i)
  !    enddo
  ! enddo
  do i = 1, n
     do j = i, n
        if(abs(iA_org(i, j) - A_res(i, j)) >= 1.0d-7) write(*, *) 'Check_diff', i, j, iA_org(i, j), A_res(i, j), abs(iA_org(i, j)-A_res(i, j))!, pivot(i)
     enddo
  enddo
  
end subroutine check_dense

subroutine check_dense_para(B_IC, matA, n, abmc)
  use coefficient
  use mod_parallel
  implicit none
  integer, intent(in) :: n
  type(BCRS_mat), intent(in) :: B_IC
  type(CRS_mat), intent(in) :: matA
  type(ord_para), intent(in) :: abmc
  double precision, allocatable :: iA_org(:, :), iA_BIC(:, :), iL(:, :), iU(:, :), iD(:, :), A_res(:, :), iD_inv(:, :)
  integer, allocatable :: pivot(:)
  integer i, j, k, l, ind_row, ind_col, icolor, jcolor

  allocate(iA_org(n, n), iA_BIC(n, n), pivot(n), iL(n, n), iU(n, n), iD(n, n), A_res(n, n), iD_inv(n, n))

  iA_org = 0.0d0
  iA_BIC = 0.0d0
  
  do j = 1, n
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        iA_org(j, matA%col_ind(i)) = matA%val(i)
     enddo
  enddo
  do j = 1, n
     do i = j+1, n
        iA_org(i, j) = iA_org(j, i)
     enddo
  enddo
  
  write(*, *) 'Original'
  do i = 1, n
     write(*, *) iA_org(i, :)
  enddo

  iL = 0.0d0
  iU = 0.0d0
  iD = 0.0d0
  do i = 1, n
     iL(i, i) = 1.0d0
     iU(i, i) = 1.0d0
     iD(i, i) = 1.0d0
  enddo

  do l = 1, B_IC%n_belem_row
     ind_row = B_IC%bsize_row * (l-1)
     do j = 1, B_IC%bsize_col
        if(ind_row+j > n) exit
        do i = 1, B_IC%bsize_row
           if(ind_row+i > n) exit
           iA_BIC(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD(ind_row+i, ind_row+j) = B_IC%dgn(i, j, l)
           iD_inv(ind_row+i, ind_row+j) = B_IC%inv_dgn(i, j, l)
        enddo
     enddo
     do k = B_IC%row_ptr(l), B_IC%row_ptr(l+1)-1
        do j = 1, B_IC%bsize_col
           if(B_IC%col_ind(k)+j-1 > n) exit
           do i = 1, B_IC%bsize_row
              if(ind_row+i > n) exit
              ! write(*, *) 'In Loop', i, j, k, ind_row+i, B_IC%col_ind(k)+j-1
              iA_BIC(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iU(ind_row+i, B_IC%col_ind(k)+j-1) = B_IC%val(i, j, k)
              iL(B_IC%col_ind(k)+j-1, ind_row+i) = B_IC%val(i, j, k)
           enddo
        enddo
     enddo
  enddo

  A_res = 0.0d0
  A_res = matmul(iL, iD)
  A_res = matmul(A_res, iU)
  
  write(*, *) 'matmul LDU'
  do i = 1, n
     write(*, *) A_res(i, :)
  enddo

  do i = 1, n
     do j = i, n
        if(abs(iA_org(i, j) - A_res(i, j)) >= 1.0d-7) write(*, *) 'Check_diff', i, j, iA_org(i, j), A_res(i, j), abs(iA_org(i, j)-A_res(i, j))!, pivot(i)
     enddo
  enddo
  
end subroutine check_dense_para

subroutine check_sol(matA, r, z, n)
  use coefficient
  ! use prepro_def
  ! use f95_lapack, only : POSV=>LA_POSV
! #ifdef gnu
!   use f95_lapack, only : POSV=>LA_POSV
! #endif
! #ifdef intel
!   use lapack95, only : POSV
! #endif
  ! use f95_lapack, only : GETRF=>LA_GETRF, GETRI=>LA_GETRI, POSV=>LA_POSV
  ! use prepro_def
  implicit none
  double precision, intent(in) :: r(:)
  double precision, intent(out) :: z(:)
  type(CRS_mat), intent(in) :: matA
  integer, intent(in) :: n
  double precision, allocatable :: iA_org(:, :)
  integer i, j, k, l, ind_row, ind_col

  allocate(iA_org(n, n))

  z = r

  iA_org = 0.0d0
  
  do j = 1, n
     do i = matA%row_ptr(j), matA%row_ptr(j+1)-1
        iA_org(j, matA%col_ind(i)) = matA%val(i)
     enddo
  enddo
  do j = 1, n
     do i = j+1, n
        iA_org(i, j) = iA_org(j, i)
     enddo
  enddo

  call POSV(iA_org, z)
  
end subroutine check_sol

subroutine calc_residual_half(A, z, B, r, n)
  use coefficient
  implicit none
  integer, intent(in) :: n
  double precision, intent(in) :: z(n), B(n)
  double precision, intent(out) :: r(n)
  type(CRS_mat), intent(in) :: A
  integer i, j, k

  r = B

  do j = 1, n
     do i = A%row_ptr(j), A%row_ptr(j+1)-1
        r(j) = r(j) - A%val(i) * z(A%col_ind(i))
     enddo
     do i = A%row_ptr(j)+1, A%row_ptr(j+1)-1
        r(A%col_ind(i)) = r(A%col_ind(i)) - A%val(i) * z(j)
     enddo
  enddo
        
end subroutine Calc_Residual_Half

recursive subroutine quicksort_blk(master, slave, val, n, size_row, size_col)
  implicit none
  integer, intent(in) :: n, size_row, size_col
  integer, intent(inout) :: master(:), slave(:)
  double precision, intent(inout) :: val(:, :, :)
  integer i, j, pivot, temp_i
  double precision temp_r(size_row, size_col)
  logical flag

  flag = .false.
  temp_i = master(1)
  do i = 2, n
     if(temp_i /= master(i)) then
        flag = .true.
        exit
     endif
  enddo

  if(.not. flag) return

  pivot = master(n/2) !If pivot is minimum, fail sorting. This part is exclude the situation.
  if(pivot == minval(master)) then
     do i = 1, n
        if(master(i) > pivot) then
           pivot = master(i)
           exit
        endif
     enddo
  endif

  i = 1
  j = n
  do while (i <= j)
     
     do while (master(i) < pivot .and. i < n)
        i = i + 1
     enddo

     do while (pivot <= master(j) .and. j > 1) 
        j = j - 1
     enddo

     if (i > j) exit

     temp_i = master(i)
     master(i) = master(j)
     master(j) = temp_i

     temp_i = slave(i)
     slave(i) = slave(j)
     slave(j) = temp_i

     temp_r = val(:, :, i)
     val(:, :, i) = val(:, :, j)
     val(:, :, j) = temp_r
     
     i = i + 1
     j = j - 1

  enddo

  !if(slave(1) == 109) then
  !   write(*, *) '###################################'
  !   write(*, *) 'pivot is ', pivot, n, i, j
  !   write(*, *) master
  !endif
  
  !write(*, *) 'Check after', i, j, n
  if (i-1 >= 2) call quicksort_blk(master(1:i-1), slave(1:i-1), val(:, :, 1:i-1), i-1, size_row, size_col)
  if (n - 1 + 1 >= 2) call quicksort_blk(master(i:n), slave(i:n), val(:, :, i:n), n - i + 1, size_row, size_col) 
  
end subroutine quicksort_blk
