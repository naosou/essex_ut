#SYSTEM = FX10
#SYSTEM = INTEL
#SYSTEM = SystemA
#SYSTEM = GNU
#SYSTEM=GNUdebug
SYSTEM=GNUdebug_mpi
#SYSTEM=INTELdebug

#FX10
ifeq ($(SYSTEM),FX10)
OPTFLAGS = -fs
CC=mpifccpx
F90=mpifrtpx -Kfast,openmp
#F90=mpifrtpx -Kopenmp
CCFLAGS = $(OPTFLAGS)
F90FLAGS = $(OPTFLAGS) -Cfpp
LDFLAGS = -SSL2
endif

#SystemA
ifeq ($(SYSTEM),SystemA)
OPTFLAGS = -O3 -homp
CC=cc
F90=ftn
CCFLAGS = $(OPTFLAGS)
F90FLAGS = $(OPTFLAGS)
endif

#intel
ifeq ($(SYSTEM),INTEL)
#OPTFLAGS = -O3 -traceback -ip -heap-arrays -openmp
OPTFLAGS = -O3 -xHost -Dintel -qopenmp -fpp -ipo -I ~/original/usr/lib/intel64/libmkl_lapack95_lp64.a -mkl
#OPTFLAGS = -check all -g -traceback -openmp
CC=icc
F90=ifort
CCFLAGS = $(OPTFLAGS)
#F90FLAGS = $(OPTFLAGS) -fpp -assume nounderscore -names uppercase
F90FLAGS = $(OPTFLAGS)
#F90FLAGS = $(OPTFLAGS) -fpp -check all
#F90FLAGS = -fpe0 -traceback -g -CB -assume nounderscore -names lowercase -fpp -check all
#LDFLAGS = -mkl -trace
LDFLAGS = $(OPTFLAGS) ~/original/usr/lib/intel64/libmkl_lapack95_lp64.a
endif

#intel
ifeq ($(SYSTEM),INTELdebug)
#OPTFLAGS = -O3 -traceback -ip -heap-arrays -openmp
#OPTFLAGS = -O3 -xHost -Dintel -qopenmp -fpp -ipo -I ~/original/usr/lib/intel64/libmkl_lapack95_lp64.a -mkl
OPTFLAGS = -check all -g -traceback -qopenmp -Dintel -fpp -ipo -I ~/original/usr/lib/intel64/libmkl_lapack95_lp64.a -mkl
CC=icc
F90=ifort
CCFLAGS = $(OPTFLAGS)
#F90FLAGS = $(OPTFLAGS) -fpp -assume nounderscore -names uppercase
F90FLAGS = $(OPTFLAGS)
#F90FLAGS = $(OPTFLAGS) -fpp -check all
#F90FLAGS = -fpe0 -traceback -g -CB -assume nounderscore -names lowercase -fpp -check all
#LDFLAGS = -mkl -trace
LDFLAGS = $(OPTFLAGS) ~/original/usr/lib/intel64/libmkl_lapack95_lp64.a
endif

#GNU
ifeq ($(SYSTEM),GNU)
F90=mpif90
CCFLAGS=$(OPTFLAGS)
F90FLAGS= -Dgnu -cpp -fopenmp -ffree-form -mcmodel=medium -ffree-line-length-none -g -O3 -llapack95 -llapack -lblas -I /usr/local/lib64/lapack95/lapack95_modules/
LDFLAGS= -Dgnu -cpp -fopenmp -ffree-form -mcmodel=medium -ffree-line-length-none -g -O3 -llapack95 -llapack -lblas -I /usr/local/lib64/lapack95/lapack95_modules/ ./liblapack95.a ./libmetis.a
endif

#GNU_debug
ifeq ($(SYSTEM),GNUdebug)
#F90=gfortran
F90=mpif90
F90FLAGS= -Dgnu -cpp -fopenmp -ffree-form -fbounds-check -O -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -mcmodel=medium -ffree-line-length-none -g -llapack95 -llapack -lblas -I /usr/local/lib64/lapack95/lapack95_modules/
LDFLAGS= -ffree-form -fopenmp -fbounds-check -O -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -mcmodel=medium -ffree-line-length-none -g -llapack95 -llapack -lblas -I /usr/local/lib64/lapack95/lapack95_modules/ ./liblapack95.a ./libmetis.a
endif

#GNU_debug
ifeq ($(SYSTEM),GNUdebug_mpi)
#F90=gfortran
F90=mpif90
F90FLAGS= -Dgnu -cpp -fopenmp -ffree-form -fbounds-check -O0 -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -mcmodel=medium -ffree-line-length-none -g -llapack95 -llapack -lblas -I /usr/local/lib64/lapack95/lapack95_modules/
LDFLAGS= -ffree-form -fopenmp -fbounds-check -O0 -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -mcmodel=medium -ffree-line-length-none -g -llapack95 -llapack -lblas -I /usr/local/lib64/lapack95/lapack95_modules/ ./liblapack95.a ./libmetis.a
endif

LINK=$(F90)

OBJS= init_parallel.o main.o ICCG_original_normalize.o read_args.o module_iccg.o #prepro_def.o

TARGET=block_ICCG_mpi

.SUFFIXES:
.SUFFIXES: .o .f90 .FPP

$(TARGET): $(OBJS)
			$(LINK) -o $@ $(OBJS) $(LDFLAGS)

.f90.o:

			$(F90) $(F90FLAGS) -c $<

#ICCG_origina_addshift.o : prepro_def.mod
main.o : module_iccg.o init_parallel.o
#read_flags.o : ICCG_original_normalize.o
ICCG_original_normalize.o : read_args.o init_parallel.o module_iccg.o
init_parallel.o : read_args.o module_iccg.o

clean:
	rm -f $(OBJS) $(OBJS:.o=.mod) $(TARGET)

